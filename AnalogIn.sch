EESchema Schematic File Version 4
LIBS:Capstone-cache
EELAYER 29 0
EELAYER END
$Descr USLetter 11000 8500
encoding utf-8
Sheet 3 8
Title "Capstone"
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text Notes 6300 7150 0    50   ~ 0
(3) DGND being connected to REF is ok because it will never be switched during a conversion.\n    -Vs + 4.25V < DGND < +Vs - 2.7V
$Comp
L Transistor_FET:2N7002 Q2
U 1 1 5CD6A231
P 2250 6650
F 0 "Q2" H 2455 6696 50  0000 L CNN
F 1 "BSS138" H 2455 6605 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 2450 6575 50  0001 L CIN
F 3 "https://assets.nexperia.com/documents/data-sheet/BSS138P.pdf" H 2250 6650 50  0001 L CNN
F 4 "Nexperia" H 0   0   50  0001 C CNN "Manufacturer"
F 5 "BSS138P,215" H 0   0   50  0001 C CNN "PN"
	1    2250 6650
	1    0    0    -1  
$EndComp
$Comp
L Transistor_FET:2N7002 Q4
U 1 1 5CD6A39D
P 1750 7050
F 0 "Q4" H 1955 7096 50  0000 L CNN
F 1 "BSS138" H 1955 7005 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 1950 6975 50  0001 L CIN
F 3 "https://assets.nexperia.com/documents/data-sheet/BSS138P.pdf" H 1750 7050 50  0001 L CNN
F 4 "Nexperia" H 0   0   50  0001 C CNN "Manufacturer"
F 5 "BSS138P,215" H 0   0   50  0001 C CNN "PN"
	1    1750 7050
	1    0    0    -1  
$EndComp
Wire Wire Line
	1850 6850 1850 6650
Wire Wire Line
	1850 6650 2050 6650
$Comp
L Device:R R14
U 1 1 5CD732A3
P 1850 6200
F 0 "R14" H 1920 6246 50  0000 L CNN
F 1 "10k" H 1920 6155 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 1780 6200 50  0001 C CNN
F 3 "~" H 1850 6200 50  0001 C CNN
F 4 "Yageo" H 0   0   50  0001 C CNN "Manufacturer"
F 5 "RC0603JR-0710K" H 0   0   50  0001 C CNN "PN"
F 6 "5%" H 0   0   50  0001 C CNN "Tolerance"
	1    1850 6200
	1    0    0    -1  
$EndComp
$Comp
L Device:R R15
U 1 1 5CD73311
P 2350 6200
F 0 "R15" H 2420 6246 50  0000 L CNN
F 1 "10k" H 2420 6155 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 2280 6200 50  0001 C CNN
F 3 "~" H 2350 6200 50  0001 C CNN
F 4 "Yageo" H 0   0   50  0001 C CNN "Manufacturer"
F 5 "RC0603JR-0710K" H 0   0   50  0001 C CNN "PN"
F 6 "5%" H 0   0   50  0001 C CNN "Tolerance"
	1    2350 6200
	1    0    0    -1  
$EndComp
Wire Wire Line
	2350 6350 2350 6400
Wire Wire Line
	1850 6350 1850 6650
Connection ~ 1850 6650
Wire Wire Line
	1850 6050 1850 5950
Wire Wire Line
	2350 6050 2350 5950
Wire Wire Line
	2350 7250 2350 6850
Connection ~ 2350 6400
Wire Wire Line
	2350 6400 2350 6450
Text Label 2750 6400 2    50   ~ 0
A[1]
Wire Wire Line
	2350 6400 2750 6400
Text HLabel 1550 7050 0    50   Input ~ 0
GAIN[1]
Text Label 7400 1650 0    50   ~ 0
INA+
Text Label 7400 2050 0    50   ~ 0
INA-
Text Notes 2650 5650 0    50   ~ 0
Gain adjust level shifters\n\nGAIN[1:0]\n  00 => G = 1\n  01 => G = 10\n  10 => G = 100\n  11 => G = 1000
$Comp
L Device:C_Small C?
U 1 1 5C7AD7C1
P 2250 1950
AR Path="/5C4E1326/5C7AD7C1" Ref="C?"  Part="1" 
AR Path="/5C5F098F/5C5F0B20/5C7AD7C1" Ref="C2"  Part="1" 
F 0 "C2" V 2150 1950 50  0000 C CNN
F 1 "47n" V 2350 1950 50  0000 C CNN
F 2 "Capacitor_SMD:C_1210_3225Metric" H 2250 1950 50  0001 C CNN
F 3 "https://content.kemet.com/datasheets/KEM_F3082_LDB.pdf" H 2250 1950 50  0001 C CNN
F 4 "Kemet" H 0   0   50  0001 C CNN "Manufacturer"
F 5 "LDBCB2470GC5N0" H 0   0   50  0001 C CNN "PN"
F 6 "2%" H 0   0   50  0001 C CNN "Tolerance"
F 7 "PPS" H 2250 1950 50  0001 C CNN "Dielectric"
F 8 "50V" H 2250 1950 50  0001 C CNN "Value2"
	1    2250 1950
	0    1    1    0   
$EndComp
Wire Wire Line
	1950 1950 2150 1950
Wire Wire Line
	2150 1550 1950 1550
Wire Wire Line
	2350 1550 2550 1550
Connection ~ 2550 1550
$Comp
L Transistor_FET:2N7002 Q1
U 1 1 5CA1393C
P 1350 2850
F 0 "Q1" H 1555 2896 50  0000 L CNN
F 1 "BSS138" H 1555 2805 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 1550 2775 50  0001 L CIN
F 3 "https://assets.nexperia.com/documents/data-sheet/BSS138P.pdf" H 1350 2850 50  0001 L CNN
F 4 "Nexperia" H -100 -500 50  0001 C CNN "Manufacturer"
F 5 "BSS138P,215" H -100 -500 50  0001 C CNN "PN"
	1    1350 2850
	-1   0    0    -1  
$EndComp
$Comp
L Device:R_Small R13
U 1 1 5CA29202
P 1650 3050
F 0 "R13" H 1709 3096 50  0000 L CNN
F 1 "100k" H 1709 3005 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 1650 3050 50  0001 C CNN
F 3 "~" H 1650 3050 50  0001 C CNN
F 4 "Yageo" H -100 -500 50  0001 C CNN "Manufacturer"
F 5 "RC0603JR-07100R" H -100 -500 50  0001 C CNN "PN"
F 6 "5%" H -100 -500 50  0001 C CNN "Tolerance"
	1    1650 3050
	1    0    0    -1  
$EndComp
Wire Wire Line
	1650 3150 1650 3250
Wire Wire Line
	1650 2950 1650 2850
Wire Wire Line
	1500 2550 1250 2550
Wire Wire Line
	1750 2850 1650 2850
Connection ~ 1650 2850
Wire Wire Line
	1650 2850 1550 2850
Text Notes 6300 6950 0    50   ~ 0
(4) Coil draws 23mA when energized
Text Notes 6300 6750 0    50   ~ 0
(5) Pulldown resistor keeps the coil off
Text Notes 6300 6850 0    50   ~ 0
(5) BAT54SW may be used instead
Text HLabel 3300 7100 0    50   Input ~ 0
GAIN[0]
Wire Wire Line
	4100 6450 4500 6450
Text Label 4500 6450 2    50   ~ 0
A[0]
Wire Wire Line
	4100 6450 4100 6500
Wire Wire Line
	4100 7300 4100 6900
Wire Wire Line
	4100 6100 4100 6000
Wire Wire Line
	3600 6100 3600 6000
Wire Wire Line
	3600 6400 3600 6700
Connection ~ 4100 6450
Wire Wire Line
	4100 6400 4100 6450
$Comp
L Device:R R17
U 1 1 5CDDC894
P 4100 6250
F 0 "R17" H 4170 6296 50  0000 L CNN
F 1 "10k" H 4170 6205 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 4030 6250 50  0001 C CNN
F 3 "~" H 4100 6250 50  0001 C CNN
F 4 "Yageo" H 0   0   50  0001 C CNN "Manufacturer"
F 5 "RC0603JR-0710K" H 0   0   50  0001 C CNN "PN"
F 6 "5%" H 0   0   50  0001 C CNN "Tolerance"
	1    4100 6250
	1    0    0    -1  
$EndComp
$Comp
L Device:R R16
U 1 1 5CDDC88D
P 3600 6250
F 0 "R16" H 3670 6296 50  0000 L CNN
F 1 "10k" H 3670 6205 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 3530 6250 50  0001 C CNN
F 3 "~" H 3600 6250 50  0001 C CNN
F 4 "Yageo" H 0   0   50  0001 C CNN "Manufacturer"
F 5 "RC0603JR-0710K" H 0   0   50  0001 C CNN "PN"
F 6 "5%" H 0   0   50  0001 C CNN "Tolerance"
	1    3600 6250
	1    0    0    -1  
$EndComp
Wire Wire Line
	3600 6700 3800 6700
Connection ~ 3600 6700
Wire Wire Line
	3600 6900 3600 6700
$Comp
L Transistor_FET:2N7002 Q5
U 1 1 5CDDC878
P 3500 7100
F 0 "Q5" H 3705 7146 50  0000 L CNN
F 1 "BSS138" H 3705 7055 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 3700 7025 50  0001 L CIN
F 3 "https://assets.nexperia.com/documents/data-sheet/BSS138P.pdf" H 3500 7100 50  0001 L CNN
F 4 "Nexperia" H 0   0   50  0001 C CNN "Manufacturer"
F 5 "BSS138P,215" H 0   0   50  0001 C CNN "PN"
	1    3500 7100
	1    0    0    -1  
$EndComp
$Comp
L Transistor_FET:2N7002 Q3
U 1 1 5CDDC871
P 4000 6700
F 0 "Q3" H 4205 6746 50  0000 L CNN
F 1 "BSS138" H 4205 6655 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 4200 6625 50  0001 L CIN
F 3 "https://assets.nexperia.com/documents/data-sheet/BSS138P.pdf" H 4000 6700 50  0001 L CNN
F 4 "Nexperia" H 0   0   50  0001 C CNN "Manufacturer"
F 5 "BSS138P,215" H 0   0   50  0001 C CNN "PN"
	1    4000 6700
	1    0    0    -1  
$EndComp
Text Notes 6300 7350 0    50   ~ 0
(1) Low cutoff in AC mode: ~~30Hz
Wire Wire Line
	3850 1550 3950 1550
Wire Wire Line
	3850 1750 3950 1750
Wire Wire Line
	4450 1950 3850 1950
Wire Wire Line
	4450 2150 3850 2150
Wire Wire Line
	4250 1350 4250 1250
Wire Wire Line
	4250 1250 4750 1250
Wire Wire Line
	4750 1250 4750 1750
Wire Wire Line
	4850 1250 4750 1250
Text HLabel 5650 3550 2    50   Output ~ 0
~FAULT
Wire Wire Line
	5650 3550 5550 3550
Wire Wire Line
	5100 2950 5100 2900
Wire Wire Line
	5300 2950 5300 2900
Wire Wire Line
	5300 2900 5100 2900
Wire Wire Line
	5300 2850 5300 2900
Connection ~ 5300 2900
Wire Wire Line
	4850 3550 4850 2900
Wire Wire Line
	4850 2900 5100 2900
Connection ~ 5100 2900
Wire Wire Line
	7800 2950 7800 3000
Wire Wire Line
	7800 3000 7550 3000
Wire Wire Line
	7550 3000 7550 3550
Connection ~ 7800 3000
Wire Wire Line
	7800 3000 7800 3050
Text Notes 2850 2850 2    50   ~ 0
(1)
Text Notes 1600 3100 2    50   ~ 0
(2)
Text Notes 1300 1200 2    50   ~ 0
(4)
Text Notes 1800 2500 0    50   ~ 0
(5)
Text Notes 4850 1400 0    50   ~ 0
(2)
Text Notes 6300 7250 0    50   ~ 0
(2) Default to DC coupling
Connection ~ 4750 1250
$Comp
L power:-5VA #PWR058
U 1 1 5C77DD0A
P 5100 4250
F 0 "#PWR058" H 5100 4350 50  0001 C CNN
F 1 "-5VA" H 5100 4400 50  0000 C CNN
F 2 "" H 5100 4250 50  0001 C CNN
F 3 "" H 5100 4250 50  0001 C CNN
	1    5100 4250
	-1   0    0    1   
$EndComp
$Comp
L power:-5VA #PWR055
U 1 1 5C77E06D
P 7700 4150
F 0 "#PWR055" H 7700 4250 50  0001 C CNN
F 1 "-5VA" H 7700 4300 50  0000 C CNN
F 2 "" H 7700 4150 50  0001 C CNN
F 3 "" H 7700 4150 50  0001 C CNN
	1    7700 4150
	-1   0    0    1   
$EndComp
Wire Wire Line
	2950 2250 2950 1950
$Comp
L power:GNDA #PWR048
U 1 1 5C7A27E7
P 2950 2750
F 0 "#PWR048" H 2950 2500 50  0001 C CNN
F 1 "GNDA" H 2950 2600 50  0000 C CNN
F 2 "" H 2950 2750 50  0001 C CNN
F 3 "" H 2950 2750 50  0001 C CNN
	1    2950 2750
	1    0    0    -1  
$EndComp
$Comp
L power:GNDD #PWR053
U 1 1 5C7AE7E9
P 1650 3250
F 0 "#PWR053" H 1650 3000 50  0001 C CNN
F 1 "GNDD" H 1650 3100 50  0000 C CNN
F 2 "" H 1650 3250 50  0001 C CNN
F 3 "" H 1650 3250 50  0001 C CNN
	1    1650 3250
	1    0    0    -1  
$EndComp
$Comp
L power:GNDD #PWR052
U 1 1 5C7AE83F
P 1250 3250
F 0 "#PWR052" H 1250 3000 50  0001 C CNN
F 1 "GNDD" H 1250 3100 50  0000 C CNN
F 2 "" H 1250 3250 50  0001 C CNN
F 3 "" H 1250 3250 50  0001 C CNN
	1    1250 3250
	1    0    0    -1  
$EndComp
Wire Wire Line
	1250 3250 1250 3050
$Comp
L power:GNDA #PWR065
U 1 1 5C7B44E8
P 2350 7250
F 0 "#PWR065" H 2350 7000 50  0001 C CNN
F 1 "GNDA" H 2350 7100 50  0000 C CNN
F 2 "" H 2350 7250 50  0001 C CNN
F 3 "" H 2350 7250 50  0001 C CNN
	1    2350 7250
	1    0    0    -1  
$EndComp
$Comp
L power:GNDD #PWR064
U 1 1 5C7B46BE
P 1850 7250
F 0 "#PWR064" H 1850 7000 50  0001 C CNN
F 1 "GNDD" H 1850 7100 50  0000 C CNN
F 2 "" H 1850 7250 50  0001 C CNN
F 3 "" H 1850 7250 50  0001 C CNN
	1    1850 7250
	1    0    0    -1  
$EndComp
$Comp
L power:GNDA #PWR067
U 1 1 5C7BAD9F
P 4100 7300
F 0 "#PWR067" H 4100 7050 50  0001 C CNN
F 1 "GNDA" H 4100 7150 50  0000 C CNN
F 2 "" H 4100 7300 50  0001 C CNN
F 3 "" H 4100 7300 50  0001 C CNN
	1    4100 7300
	1    0    0    -1  
$EndComp
$Comp
L power:GNDD #PWR066
U 1 1 5C7BADA5
P 3600 7300
F 0 "#PWR066" H 3600 7050 50  0001 C CNN
F 1 "GNDD" H 3600 7150 50  0000 C CNN
F 2 "" H 3600 7300 50  0001 C CNN
F 3 "" H 3600 7300 50  0001 C CNN
	1    3600 7300
	1    0    0    -1  
$EndComp
$Comp
L power:+5VA #PWR050
U 1 1 5C7C1C8B
P 7800 2950
F 0 "#PWR050" H 7800 2800 50  0001 C CNN
F 1 "+5VA" H 7800 3100 50  0000 C CNN
F 2 "" H 7800 2950 50  0001 C CNN
F 3 "" H 7800 2950 50  0001 C CNN
	1    7800 2950
	1    0    0    -1  
$EndComp
$Comp
L power:+5VA #PWR049
U 1 1 5C7C1D8A
P 5300 2850
F 0 "#PWR049" H 5300 2700 50  0001 C CNN
F 1 "+5VA" H 5300 3000 50  0000 C CNN
F 2 "" H 5300 2850 50  0001 C CNN
F 3 "" H 5300 2850 50  0001 C CNN
	1    5300 2850
	1    0    0    -1  
$EndComp
$Comp
L power:+5VA #PWR062
U 1 1 5C7BFFB6
P 3600 6000
F 0 "#PWR062" H 3600 5850 50  0001 C CNN
F 1 "+5VA" H 3600 6150 50  0000 C CNN
F 2 "" H 3600 6000 50  0001 C CNN
F 3 "" H 3600 6000 50  0001 C CNN
	1    3600 6000
	1    0    0    -1  
$EndComp
$Comp
L power:+5VA #PWR063
U 1 1 5C7BFFB0
P 4100 6000
F 0 "#PWR063" H 4100 5850 50  0001 C CNN
F 1 "+5VA" H 4100 6150 50  0000 C CNN
F 2 "" H 4100 6000 50  0001 C CNN
F 3 "" H 4100 6000 50  0001 C CNN
	1    4100 6000
	1    0    0    -1  
$EndComp
$Comp
L power:+5VA #PWR060
U 1 1 5C7B529C
P 1850 5950
F 0 "#PWR060" H 1850 5800 50  0001 C CNN
F 1 "+5VA" H 1850 6100 50  0000 C CNN
F 2 "" H 1850 5950 50  0001 C CNN
F 3 "" H 1850 5950 50  0001 C CNN
	1    1850 5950
	1    0    0    -1  
$EndComp
$Comp
L power:+5VA #PWR061
U 1 1 5C7B51F1
P 2350 5950
F 0 "#PWR061" H 2350 5800 50  0001 C CNN
F 1 "+5VA" H 2350 6100 50  0000 C CNN
F 2 "" H 2350 5950 50  0001 C CNN
F 3 "" H 2350 5950 50  0001 C CNN
	1    2350 5950
	1    0    0    -1  
$EndComp
$Comp
L power:GNDD #PWR059
U 1 1 5CCA201A
P 5400 4250
F 0 "#PWR059" H 5400 4000 50  0001 C CNN
F 1 "GNDD" H 5400 4100 50  0000 C CNN
F 2 "" H 5400 4250 50  0001 C CNN
F 3 "" H 5400 4250 50  0001 C CNN
	1    5400 4250
	1    0    0    -1  
$EndComp
Wire Wire Line
	5400 4250 5400 4200
Wire Wire Line
	5400 4200 5300 4200
Wire Wire Line
	5300 4200 5300 4150
Wire Wire Line
	5100 4150 5100 4200
Wire Wire Line
	5200 4200 5200 4150
Connection ~ 5100 4200
Wire Wire Line
	5100 4200 5100 4250
$Comp
L power:GNDD #PWR056
U 1 1 5CCC8AF9
P 8000 4150
F 0 "#PWR056" H 8000 3900 50  0001 C CNN
F 1 "GNDD" H 8000 4000 50  0000 C CNN
F 2 "" H 8000 4150 50  0001 C CNN
F 3 "" H 8000 4150 50  0001 C CNN
	1    8000 4150
	1    0    0    -1  
$EndComp
Wire Wire Line
	8000 4150 8000 4100
Wire Wire Line
	8000 4100 7900 4100
Wire Wire Line
	7900 4100 7900 4050
Wire Wire Line
	7800 4050 7800 4100
Wire Wire Line
	7800 4100 7700 4100
Wire Wire Line
	7700 4100 7700 4150
$Comp
L Device:C_Small C?
U 1 1 5C7AD7BA
P 2250 1550
AR Path="/5C4E1326/5C7AD7BA" Ref="C?"  Part="1" 
AR Path="/5C5F098F/5C5F0B20/5C7AD7BA" Ref="C1"  Part="1" 
F 0 "C1" V 2150 1550 50  0000 C CNN
F 1 "47n" V 2350 1550 50  0000 C CNN
F 2 "Capacitor_SMD:C_1210_3225Metric" H 2250 1550 50  0001 C CNN
F 3 "https://content.kemet.com/datasheets/KEM_F3082_LDB.pdf" H 2250 1550 50  0001 C CNN
F 4 "Kemet" H 0   0   50  0001 C CNN "Manufacturer"
F 5 "LDBCB2470GC5N0" H 0   0   50  0001 C CNN "PN"
F 6 "2%" H 0   0   50  0001 C CNN "Tolerance"
F 7 "PPS" H 2250 1550 50  0001 C CNN "Dielectric"
F 8 "50V" H 2250 1550 50  0001 C CNN "Value2"
	1    2250 1550
	0    1    1    0   
$EndComp
Text HLabel 1750 2850 2    50   Input ~ 0
AC_MODE
Text HLabel 4850 1250 2    50   Input ~ 0
AC_MODE
$Comp
L Capstone:ADG5462FxRU U8
U 1 1 5D2AC2A4
P 3500 1550
F 0 "U8" H 3850 1500 50  0000 C CNN
F 1 "ADG5462FxRU" H 3500 1701 50  0001 C CNN
F 2 "Package_SO:TSSOP-16_4.4x5mm_P0.65mm" H 3525 525 50  0001 C CNN
F 3 "https://www.analog.com/media/en/technical-documentation/data-sheets/ADG5462F.pdf" H 3475 425 50  0001 C CNN
F 4 "Analog Devices" H 0   0   50  0001 C CNN "Manufacturer"
F 5 "ADG5462FBRUZ" H 0   0   50  0001 C CNN "PN"
	1    3500 1550
	1    0    0    -1  
$EndComp
$Comp
L Capstone:ADG5462FxRU U8
U 4 1 5D2AD1E1
P 3500 1750
F 0 "U8" H 3850 1700 50  0000 C CNN
F 1 "ADG5462FxRU" H 3500 1901 50  0001 C CNN
F 2 "Package_SO:TSSOP-16_4.4x5mm_P0.65mm" H 3525 725 50  0001 C CNN
F 3 "https://www.analog.com/media/en/technical-documentation/data-sheets/ADG5462F.pdf" H 3475 625 50  0001 C CNN
F 4 "Analog Devices" H 0   0   50  0001 C CNN "Manufacturer"
F 5 "ADG5462FBRUZ" H 0   0   50  0001 C CNN "PN"
	4    3500 1750
	1    0    0    -1  
$EndComp
$Comp
L Capstone:ADG5462FxRU U8
U 2 1 5D2AE4F5
P 3500 1950
F 0 "U8" H 3850 1900 50  0000 C CNN
F 1 "ADG5462FxRU" H 3500 2101 50  0001 C CNN
F 2 "Package_SO:TSSOP-16_4.4x5mm_P0.65mm" H 3525 925 50  0001 C CNN
F 3 "https://www.analog.com/media/en/technical-documentation/data-sheets/ADG5462F.pdf" H 3475 825 50  0001 C CNN
F 4 "Analog Devices" H 0   0   50  0001 C CNN "Manufacturer"
F 5 "ADG5462FBRUZ" H 0   0   50  0001 C CNN "PN"
	2    3500 1950
	1    0    0    -1  
$EndComp
$Comp
L Capstone:ADG5462FxRU U8
U 3 1 5D2AF0E6
P 3500 2150
F 0 "U8" H 3850 2100 50  0000 C CNN
F 1 "ADG5462FxRU" H 3500 2301 50  0001 C CNN
F 2 "Package_SO:TSSOP-16_4.4x5mm_P0.65mm" H 3525 1125 50  0001 C CNN
F 3 "https://www.analog.com/media/en/technical-documentation/data-sheets/ADG5462F.pdf" H 3475 1025 50  0001 C CNN
F 4 "Analog Devices" H 0   0   50  0001 C CNN "Manufacturer"
F 5 "ADG5462FBRUZ" H 0   0   50  0001 C CNN "PN"
	3    3500 2150
	1    0    0    -1  
$EndComp
$Comp
L Capstone:ADG1636xRU U9
U 1 1 5D2B12B4
P 4250 1750
F 0 "U9" H 4250 1900 50  0000 C CNN
F 1 "ADG1636xRU" H 4250 1990 50  0001 C CNN
F 2 "Package_SO:TSSOP-16_4.4x5mm_P0.65mm" H 4250 900 50  0001 C CNN
F 3 "https://www.analog.com/media/en/technical-documentation/data-sheets/ADG1636.pdf" H 3200 1300 50  0001 C CNN
F 4 "Analog Devices" H 0   0   50  0001 C CNN "Manufacturer"
F 5 "ADG1636BRUZ" H 0   0   50  0001 C CNN "PN"
	1    4250 1750
	-1   0    0    1   
$EndComp
$Comp
L Capstone:ADG1636xRU U9
U 2 1 5D2B2D5E
P 4750 2150
F 0 "U9" H 4750 2299 50  0000 C CNN
F 1 "ADG1636xRU" H 4750 2390 50  0000 C CNN
F 2 "Package_SO:TSSOP-16_4.4x5mm_P0.65mm" H 4750 1300 50  0001 C CNN
F 3 "https://www.analog.com/media/en/technical-documentation/data-sheets/ADG1636.pdf" H 3700 1700 50  0001 C CNN
F 4 "Analog Devices" H 0   0   50  0001 C CNN "Manufacturer"
F 5 "ADG1636BRUZ" H 0   0   50  0001 C CNN "PN"
	2    4750 2150
	-1   0    0    1   
$EndComp
$Comp
L Capstone:ADG5462FxRU U8
U 5 1 5D2B627D
P 5200 3550
F 0 "U8" H 5450 3950 50  0000 L CNN
F 1 "ADG5462FxRU" H 5450 3850 50  0000 L CNN
F 2 "Package_SO:TSSOP-16_4.4x5mm_P0.65mm" H 5225 2525 50  0001 C CNN
F 3 "https://www.analog.com/media/en/technical-documentation/data-sheets/ADG5462F.pdf" H 5175 2425 50  0001 C CNN
F 4 "Analog Devices" H 1300 0   50  0001 C CNN "Manufacturer"
F 5 "ADG5462FBRUZ" H 1300 0   50  0001 C CNN "PN"
	5    5200 3550
	1    0    0    -1  
$EndComp
$Comp
L Capstone:ADG1636xRU U9
U 3 1 5D2B97E7
P 7800 3550
F 0 "U9" H 8030 3596 50  0000 L CNN
F 1 "ADG1636xRU" H 8030 3505 50  0000 L CNN
F 2 "Package_SO:TSSOP-16_4.4x5mm_P0.65mm" H 7800 2700 50  0001 C CNN
F 3 "https://www.analog.com/media/en/technical-documentation/data-sheets/ADG1636.pdf" H 6750 3100 50  0001 C CNN
F 4 "Analog Devices" H 2700 0   50  0001 C CNN "Manufacturer"
F 5 "ADG1636BRUZ" H 2700 0   50  0001 C CNN "PN"
	3    7800 3550
	1    0    0    -1  
$EndComp
Wire Wire Line
	2950 2750 2950 2650
$Comp
L Device:R R11
U 1 1 5F0D9A84
P 2550 2400
F 0 "R11" H 2480 2354 50  0000 R CNN
F 1 "120k" H 2480 2445 50  0000 R CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 2480 2400 50  0001 C CNN
F 3 "~" H 2550 2400 50  0001 C CNN
F 4 "Yageo" H 2550 2400 50  0001 C CNN "Manufacturer"
F 5 "RT0805BRD07120K" H 2550 2400 50  0001 C CNN "PN"
F 6 "0.1%" H 2550 2400 50  0001 C CNN "Tolerance"
	1    2550 2400
	-1   0    0    1   
$EndComp
Wire Wire Line
	2550 2650 2550 2550
Wire Wire Line
	2550 2250 2550 1550
Wire Wire Line
	2550 1550 3150 1550
Wire Wire Line
	1950 1750 3150 1750
Wire Wire Line
	1950 2150 3150 2150
Wire Wire Line
	2350 1950 2950 1950
Connection ~ 2950 1950
Wire Wire Line
	2950 1950 3150 1950
Wire Wire Line
	2950 2650 2550 2650
Connection ~ 2950 2650
Wire Wire Line
	2950 2650 2950 2550
$Comp
L Device:R R12
U 1 1 5F17CA74
P 2950 2400
F 0 "R12" H 2880 2354 50  0000 R CNN
F 1 "120k" H 2880 2445 50  0000 R CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 2880 2400 50  0001 C CNN
F 3 "~" H 2950 2400 50  0001 C CNN
F 4 "Yageo" H 2950 2400 50  0001 C CNN "Manufacturer"
F 5 "RT0805BRD07120K" H 2950 2400 50  0001 C CNN "PN"
F 6 "0.1%" H 2950 2400 50  0001 C CNN "Tolerance"
	1    2950 2400
	-1   0    0    1   
$EndComp
Connection ~ 9700 2350
Wire Wire Line
	9700 2450 9700 2350
$Comp
L power:GNDA #PWR047
U 1 1 5C77E9DD
P 9700 2450
F 0 "#PWR047" H 9700 2200 50  0001 C CNN
F 1 "GNDA" H 9700 2300 50  0000 C CNN
F 2 "" H 9700 2450 50  0001 C CNN
F 3 "" H 9700 2450 50  0001 C CNN
	1    9700 2450
	1    0    0    -1  
$EndComp
$Comp
L power:+5VA #PWR044
U 1 1 5C77E8BB
P 8800 1350
F 0 "#PWR044" H 8800 1200 50  0001 C CNN
F 1 "+5VA" H 8800 1500 50  0000 C CNN
F 2 "" H 8800 1350 50  0001 C CNN
F 3 "" H 8800 1350 50  0001 C CNN
	1    8800 1350
	1    0    0    -1  
$EndComp
$Comp
L power:-5VA #PWR046
U 1 1 5C77E76A
P 8800 2350
F 0 "#PWR046" H 8800 2450 50  0001 C CNN
F 1 "-5VA" H 8800 2500 50  0000 C CNN
F 2 "" H 8800 2350 50  0001 C CNN
F 3 "" H 8800 2350 50  0001 C CNN
	1    8800 2350
	-1   0    0    1   
$EndComp
Text Notes 9100 2450 2    50   ~ 0
(3)
$Comp
L Capstone:AD8253 U10
U 1 1 5CC4F48C
P 9000 1850
F 0 "U10" H 8500 2200 50  0000 L CNN
F 1 "AD8253" H 9500 1500 50  0000 R CNN
F 2 "Package_SO:MSOP-10_3x3mm_P0.5mm" H 9200 1850 50  0001 C CNN
F 3 "https://www.analog.com/media/en/technical-documentation/data-sheets/AD8253.pdf" H 9200 1850 50  0001 C CNN
F 4 "Analog Devices" H 0   0   50  0001 C CNN "Manufacturer"
F 5 "AD8253ARMZ" H 0   0   50  0001 C CNN "PN"
	1    9000 1850
	1    0    0    -1  
$EndComp
Wire Wire Line
	9200 1250 9200 1450
Wire Wire Line
	9000 1250 9200 1250
Wire Wire Line
	9000 1300 9000 1250
$Comp
L power:GND #PWR043
U 1 1 5C616E03
P 9000 1300
F 0 "#PWR043" H 9000 1050 50  0001 C CNN
F 1 "GND" H 9005 1127 50  0000 C CNN
F 2 "" H 9000 1300 50  0001 C CNN
F 3 "" H 9000 1300 50  0001 C CNN
	1    9000 1300
	1    0    0    -1  
$EndComp
Wire Wire Line
	9000 2350 9000 2250
Wire Wire Line
	9700 2350 9000 2350
Wire Wire Line
	9700 2050 9700 2350
Wire Wire Line
	9600 2050 9700 2050
Text HLabel 9800 1850 2    50   Output ~ 0
OUT
Wire Wire Line
	9600 1850 9800 1850
Wire Wire Line
	8800 1350 8800 1450
Wire Wire Line
	8800 2350 8800 2250
Wire Wire Line
	9400 1350 9400 1450
Wire Wire Line
	9700 1350 9400 1350
Text Label 9700 1350 2    50   ~ 0
A[0]
Wire Wire Line
	9300 1250 9700 1250
Wire Wire Line
	9300 1250 9300 1450
Text Label 9700 1250 2    50   ~ 0
A[1]
$Comp
L Device:R R8
U 1 1 5F192E17
P 7850 1650
F 0 "R8" V 7643 1650 50  0000 C CNN
F 1 "1.33k" V 7734 1650 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 7780 1650 50  0001 C CNN
F 3 "~" H 7850 1650 50  0001 C CNN
F 4 "Yageo" V 7850 1650 50  0001 C CNN "Manufacturer"
F 5 "RT0603BRD071K33" V 7850 1650 50  0001 C CNN "PN"
F 6 "0.1%" V 7850 1650 50  0001 C CNN "Tolerance"
	1    7850 1650
	0    1    1    0   
$EndComp
$Comp
L Device:R R9
U 1 1 5F194707
P 7850 2050
F 0 "R9" V 7643 2050 50  0000 C CNN
F 1 "1.33k" V 7734 2050 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 7780 2050 50  0001 C CNN
F 3 "~" H 7850 2050 50  0001 C CNN
F 4 "Yageo" V 7850 2050 50  0001 C CNN "Manufacturer"
F 5 "RT0603BRD071K33" V 7850 2050 50  0001 C CNN "PN"
F 6 "0.1%" V 7850 2050 50  0001 C CNN "Tolerance"
	1    7850 2050
	0    1    1    0   
$EndComp
$Comp
L Connector:TestPoint TP1
U 1 1 5F1ABB7F
P 6050 1650
F 0 "TP1" H 6000 1800 50  0000 R CNN
F 1 "IN+" H 6000 1800 50  0001 R CNN
F 2 "TestPoint:TestPoint_Pad_D1.0mm" H 6250 1650 50  0001 C CNN
F 3 "~" H 6250 1650 50  0001 C CNN
	1    6050 1650
	-1   0    0    1   
$EndComp
$Comp
L Connector:TestPoint TP3
U 1 1 5F1AB1C0
P 6050 2050
F 0 "TP3" H 6100 2200 50  0000 L CNN
F 1 "IN-" H 6100 2100 50  0001 L CNN
F 2 "TestPoint:TestPoint_Pad_D1.0mm" H 6250 2050 50  0001 C CNN
F 3 "~" H 6250 2050 50  0001 C CNN
	1    6050 2050
	1    0    0    -1  
$EndComp
Wire Wire Line
	8000 1650 8400 1650
Wire Wire Line
	8000 2050 8400 2050
$Comp
L power:GNDA #PWR045
U 1 1 5F1B967B
P 5750 2150
F 0 "#PWR045" H 5750 1900 50  0001 C CNN
F 1 "GNDA" H 5755 1977 50  0000 C CNN
F 2 "" H 5750 2150 50  0001 C CNN
F 3 "" H 5750 2150 50  0001 C CNN
	1    5750 2150
	1    0    0    -1  
$EndComp
$Comp
L Connector:TestPoint TP2
U 1 1 5F1B389E
P 5850 1850
F 0 "TP2" V 5800 1800 50  0000 L CNN
F 1 "GNDA" V 5900 1750 50  0001 L CNN
F 2 "TestPoint:TestPoint_THTPad_D1.5mm_Drill0.7mm" H 6050 1850 50  0001 C CNN
F 3 "~" H 6050 1850 50  0001 C CNN
	1    5850 1850
	0    1    1    0   
$EndComp
Connection ~ 6050 1650
Connection ~ 6050 2050
Wire Wire Line
	4550 1650 6050 1650
Wire Wire Line
	5050 2050 6050 2050
Wire Wire Line
	5750 2150 5750 1850
Wire Wire Line
	5750 1850 5850 1850
Text Notes 6300 6650 0    50   ~ 0
(6) Resistors chosen based on Ron of switches: 10Ω (ADG5462F) + 5Ω (ADG1609)
Wire Wire Line
	6050 2050 7700 2050
Wire Wire Line
	6050 1650 7700 1650
Text HLabel 1150 1650 0    50   Input ~ 0
IN+
Wire Wire Line
	1350 1650 1150 1650
Text HLabel 1150 2050 0    50   Input ~ 0
IN-
Wire Wire Line
	1350 2050 1150 2050
$Comp
L Diode:BAT54W D7
U 1 1 5CC9339B
P 1650 2550
F 0 "D7" H 1650 2450 50  0000 C CNN
F 1 "BAT54W" H 1650 2650 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-323_SC-70" H 1650 2375 50  0001 C CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/BAT54W_SER.pdf" H 1650 2550 50  0001 C CNN
F 4 "ON Semiconductor" H 1650 2550 50  0001 C CNN "Manufacturer"
F 5 "BAT54SWT1G" H 1650 2550 50  0001 C CNN "PN"
	1    1650 2550
	-1   0    0    1   
$EndComp
Wire Wire Line
	1800 2550 2050 2550
Wire Wire Line
	9600 3550 9600 3600
Connection ~ 9600 3550
Wire Wire Line
	9600 3500 9600 3550
$Comp
L Device:C C?
U 1 1 5CC6B77A
P 9600 3350
AR Path="/5C5F098F/5C88365D/5CC6B77A" Ref="C?"  Part="1" 
AR Path="/5C5F098F/5C5F0B20/5CC6B77A" Ref="C5"  Part="1" 
F 0 "C5" H 9692 3396 50  0000 L CNN
F 1 "470n" H 9692 3305 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 9600 3350 50  0001 C CNN
F 3 "~https://www.murata.com/~/media/webrenewal/support/library/catalog/products/capacitor/mlcc/c02e.ashx" H 9600 3350 50  0001 C CNN
F 4 "Murata" H 7450 -1150 50  0001 C CNN "Manufacturer"
F 5 "GRM188R71C474KA88D" H 7450 -1150 50  0001 C CNN "PN"
F 6 "10%" H 7450 -1150 50  0001 C CNN "Tolerance"
F 7 "X7R" H 9600 3350 50  0001 C CNN "Dielectric"
F 8 "16V" H 9600 3350 50  0001 C CNN "Value2"
	1    9600 3350
	1    0    0    -1  
$EndComp
Wire Wire Line
	9600 3550 10100 3550
Wire Wire Line
	10100 3550 10100 3650
$Comp
L power:GNDA #PWR?
U 1 1 5CDD451D
P 10100 3650
AR Path="/5E105304/5CDD451D" Ref="#PWR?"  Part="1" 
AR Path="/5C5F098F/5C5F0B20/5CDD451D" Ref="#PWR054"  Part="1" 
F 0 "#PWR054" H 10100 3400 50  0001 C CNN
F 1 "GNDA" H 10100 3500 50  0000 C CNN
F 2 "" H 10100 3650 50  0001 C CNN
F 3 "" H 10100 3650 50  0001 C CNN
	1    10100 3650
	1    0    0    -1  
$EndComp
$Comp
L power:+5VA #PWR?
U 1 1 5CDD4517
P 9600 3100
AR Path="/5CBF1A8E/5CDD4517" Ref="#PWR?"  Part="1" 
AR Path="/5E105304/5CDD4517" Ref="#PWR?"  Part="1" 
AR Path="/5C5F098F/5C5F0B20/5CDD4517" Ref="#PWR051"  Part="1" 
F 0 "#PWR051" H 9600 2950 50  0001 C CNN
F 1 "+5VA" H 9600 3250 50  0000 C CNN
F 2 "" H 9600 3100 50  0001 C CNN
F 3 "" H 9600 3100 50  0001 C CNN
	1    9600 3100
	1    0    0    -1  
$EndComp
$Comp
L power:-5VA #PWR?
U 1 1 5CDD4511
P 9600 4000
AR Path="/5CBF1A8E/5CDD4511" Ref="#PWR?"  Part="1" 
AR Path="/5E105304/5CDD4511" Ref="#PWR?"  Part="1" 
AR Path="/5C5F098F/5C5F0B20/5CDD4511" Ref="#PWR057"  Part="1" 
F 0 "#PWR057" H 9600 4100 50  0001 C CNN
F 1 "-5VA" H 9600 4150 50  0000 C CNN
F 2 "" H 9600 4000 50  0001 C CNN
F 3 "" H 9600 4000 50  0001 C CNN
	1    9600 4000
	-1   0    0    1   
$EndComp
$Comp
L Device:C C40
U 1 1 5D1CBA21
P 4450 3550
F 0 "C40" H 4565 3596 50  0000 L CNN
F 1 "100n" H 4565 3505 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 4488 3400 50  0001 C CNN
F 3 "~" H 4450 3550 50  0001 C CNN
F 4 "Murata" H 4450 3550 50  0001 C CNN "Manufacturer"
F 5 "GRM188R71C104KA01D" H 4450 3550 50  0001 C CNN "PN"
F 6 "10%" H 4450 3550 50  0001 C CNN "Tolerance"
F 7 "X7R" H 4450 3550 50  0001 C CNN "Dielectric"
F 8 "16V" H 4450 3550 50  0001 C CNN "Value2"
	1    4450 3550
	1    0    0    -1  
$EndComp
Wire Wire Line
	4850 2900 4450 2900
Wire Wire Line
	4450 2900 4450 3400
Connection ~ 4850 2900
Wire Wire Line
	5100 4200 5200 4200
Wire Wire Line
	5100 4200 4450 4200
Wire Wire Line
	4450 4200 4450 3700
$Comp
L Device:C C55
U 1 1 5D1D9988
P 7150 3550
F 0 "C55" H 7265 3596 50  0000 L CNN
F 1 "100n" H 7265 3505 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 7188 3400 50  0001 C CNN
F 3 "~" H 7150 3550 50  0001 C CNN
F 4 "Murata" H 7150 3550 50  0001 C CNN "Manufacturer"
F 5 "GRM188R71C104KA01D" H 7150 3550 50  0001 C CNN "PN"
F 6 "10%" H 7150 3550 50  0001 C CNN "Tolerance"
F 7 "X7R" H 7150 3550 50  0001 C CNN "Dielectric"
F 8 "16V" H 7150 3550 50  0001 C CNN "Value2"
	1    7150 3550
	1    0    0    -1  
$EndComp
Wire Wire Line
	7550 3000 7150 3000
Wire Wire Line
	7150 3000 7150 3400
Wire Wire Line
	7150 4100 7700 4100
Wire Wire Line
	7150 3700 7150 4100
Connection ~ 7550 3000
Connection ~ 7700 4100
Wire Wire Line
	9600 3100 9600 3150
Wire Wire Line
	9600 3900 9600 3950
Wire Wire Line
	9600 3150 9200 3150
Wire Wire Line
	9200 3150 9200 3400
Wire Wire Line
	9200 3950 9600 3950
Wire Wire Line
	9200 3700 9200 3950
Connection ~ 9600 3150
Wire Wire Line
	9600 3150 9600 3200
Connection ~ 9600 3950
Wire Wire Line
	9600 3950 9600 4000
$Comp
L Device:C C?
U 1 1 5D3CC65F
P 9600 3750
AR Path="/5C5F098F/5C88365D/5D3CC65F" Ref="C?"  Part="1" 
AR Path="/5C5F098F/5C5F0B20/5D3CC65F" Ref="C56"  Part="1" 
F 0 "C56" H 9692 3796 50  0000 L CNN
F 1 "470n" H 9692 3705 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 9600 3750 50  0001 C CNN
F 3 "~https://www.murata.com/~/media/webrenewal/support/library/catalog/products/capacitor/mlcc/c02e.ashx" H 9600 3750 50  0001 C CNN
F 4 "Murata" H 7450 -750 50  0001 C CNN "Manufacturer"
F 5 "GRM188R71C474KA88D" H 7450 -750 50  0001 C CNN "PN"
F 6 "10%" H 7450 -750 50  0001 C CNN "Tolerance"
F 7 "X7R" H 9600 3750 50  0001 C CNN "Dielectric"
F 8 "16V" H 9600 3750 50  0001 C CNN "Value2"
	1    9600 3750
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 5D3CDCE2
P 9200 3550
AR Path="/5C5F098F/5C88365D/5D3CDCE2" Ref="C?"  Part="1" 
AR Path="/5C5F098F/5C5F0B20/5D3CDCE2" Ref="C10"  Part="1" 
F 0 "C10" H 9292 3596 50  0000 L CNN
F 1 "1u" H 9292 3505 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 9200 3550 50  0001 C CNN
F 3 "https://www.murata.com/~/media/webrenewal/support/library/catalog/products/capacitor/mlcc/c02e.ashx" H 9200 3550 50  0001 C CNN
F 4 "Murata" H 7050 -950 50  0001 C CNN "Manufacturer"
F 5 "GRM21BR61C105KA01K" H 7050 -950 50  0001 C CNN "PN"
F 6 "10%" H 7050 -950 50  0001 C CNN "Tolerance"
F 7 "X5R" H 9200 3550 50  0001 C CNN "Dielectric"
F 8 "16V" H 9200 3550 50  0001 C CNN "Value2"
	1    9200 3550
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 5D046815
P 3650 3550
AR Path="/5C5F098F/5C88365D/5D046815" Ref="C?"  Part="1" 
AR Path="/5C5F098F/5C5F0B20/5D046815" Ref="C68"  Part="1" 
F 0 "C68" H 3742 3596 50  0000 L CNN
F 1 "1u" H 3742 3505 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 3650 3550 50  0001 C CNN
F 3 "https://www.murata.com/~/media/webrenewal/support/library/catalog/products/capacitor/mlcc/c02e.ashx" H 3650 3550 50  0001 C CNN
F 4 "Murata" H 1500 -950 50  0001 C CNN "Manufacturer"
F 5 "GRM21BR61C105KA01K" H 1500 -950 50  0001 C CNN "PN"
F 6 "10%" H 1500 -950 50  0001 C CNN "Tolerance"
F 7 "X5R" H 3650 3550 50  0001 C CNN "Dielectric"
F 8 "16V" H 3650 3550 50  0001 C CNN "Value2"
	1    3650 3550
	1    0    0    -1  
$EndComp
Wire Wire Line
	3650 2900 3650 3400
Wire Wire Line
	4050 4200 4450 4200
Wire Wire Line
	3650 3700 3650 4200
Connection ~ 4450 2900
Connection ~ 4450 4200
$Comp
L Device:C C?
U 1 1 5D050E89
P 4050 3550
AR Path="/5C5F098F/5C88365D/5D050E89" Ref="C?"  Part="1" 
AR Path="/5C5F098F/5C5F0B20/5D050E89" Ref="C69"  Part="1" 
F 0 "C69" H 4142 3596 50  0000 L CNN
F 1 "470n" H 4142 3505 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 4050 3550 50  0001 C CNN
F 3 "~https://www.murata.com/~/media/webrenewal/support/library/catalog/products/capacitor/mlcc/c02e.ashx" H 4050 3550 50  0001 C CNN
F 4 "Murata" H 1900 -950 50  0001 C CNN "Manufacturer"
F 5 "GRM188R71C474KA88D" H 1900 -950 50  0001 C CNN "PN"
F 6 "10%" H 1900 -950 50  0001 C CNN "Tolerance"
F 7 "X7R" H 4050 3550 50  0001 C CNN "Dielectric"
F 8 "16V" H 4050 3550 50  0001 C CNN "Value2"
	1    4050 3550
	1    0    0    -1  
$EndComp
Wire Wire Line
	3650 2900 4050 2900
Wire Wire Line
	3650 4200 4050 4200
Wire Wire Line
	4050 4200 4050 3700
Wire Wire Line
	4050 3400 4050 2900
Connection ~ 4050 4200
Connection ~ 4050 2900
Wire Wire Line
	4050 2900 4450 2900
$Comp
L Device:C C?
U 1 1 5D073B56
P 6350 3550
AR Path="/5C5F098F/5C88365D/5D073B56" Ref="C?"  Part="1" 
AR Path="/5C5F098F/5C5F0B20/5D073B56" Ref="C70"  Part="1" 
F 0 "C70" H 6442 3596 50  0000 L CNN
F 1 "1u" H 6442 3505 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 6350 3550 50  0001 C CNN
F 3 "https://www.murata.com/~/media/webrenewal/support/library/catalog/products/capacitor/mlcc/c02e.ashx" H 6350 3550 50  0001 C CNN
F 4 "Murata" H 4200 -950 50  0001 C CNN "Manufacturer"
F 5 "GRM21BR61C105KA01K" H 4200 -950 50  0001 C CNN "PN"
F 6 "10%" H 4200 -950 50  0001 C CNN "Tolerance"
F 7 "X5R" H 6350 3550 50  0001 C CNN "Dielectric"
F 8 "16V" H 6350 3550 50  0001 C CNN "Value2"
	1    6350 3550
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 5D073B5F
P 6750 3550
AR Path="/5C5F098F/5C88365D/5D073B5F" Ref="C?"  Part="1" 
AR Path="/5C5F098F/5C5F0B20/5D073B5F" Ref="C71"  Part="1" 
F 0 "C71" H 6842 3596 50  0000 L CNN
F 1 "470n" H 6842 3505 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 6750 3550 50  0001 C CNN
F 3 "~https://www.murata.com/~/media/webrenewal/support/library/catalog/products/capacitor/mlcc/c02e.ashx" H 6750 3550 50  0001 C CNN
F 4 "Murata" H 4600 -950 50  0001 C CNN "Manufacturer"
F 5 "GRM188R71C474KA88D" H 4600 -950 50  0001 C CNN "PN"
F 6 "10%" H 4600 -950 50  0001 C CNN "Tolerance"
F 7 "X7R" H 6750 3550 50  0001 C CNN "Dielectric"
F 8 "16V" H 6750 3550 50  0001 C CNN "Value2"
	1    6750 3550
	1    0    0    -1  
$EndComp
Wire Wire Line
	7150 3000 6750 3000
Wire Wire Line
	6750 3000 6750 3400
Wire Wire Line
	6750 4100 7150 4100
Wire Wire Line
	6750 3700 6750 4100
Connection ~ 7150 3000
Connection ~ 7150 4100
Wire Wire Line
	6750 3000 6350 3000
Wire Wire Line
	6350 3000 6350 3400
Wire Wire Line
	6350 4100 6750 4100
Wire Wire Line
	6350 3700 6350 4100
Connection ~ 6750 3000
Connection ~ 6750 4100
$Comp
L Device:C C?
U 1 1 5CEA7148
P 8800 3550
AR Path="/5C5F098F/5C88365D/5CEA7148" Ref="C?"  Part="1" 
AR Path="/5C5F098F/5C5F0B20/5CEA7148" Ref="C72"  Part="1" 
F 0 "C72" H 8892 3596 50  0000 L CNN
F 1 "1u" H 8892 3505 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 8800 3550 50  0001 C CNN
F 3 "https://www.murata.com/~/media/webrenewal/support/library/catalog/products/capacitor/mlcc/c02e.ashx" H 8800 3550 50  0001 C CNN
F 4 "Murata" H 6650 -950 50  0001 C CNN "Manufacturer"
F 5 "GRM21BR61C105KA01K" H 6650 -950 50  0001 C CNN "PN"
F 6 "10%" H 6650 -950 50  0001 C CNN "Tolerance"
F 7 "X5R" H 8800 3550 50  0001 C CNN "Dielectric"
F 8 "16V" H 8800 3550 50  0001 C CNN "Value2"
	1    8800 3550
	1    0    0    -1  
$EndComp
Wire Wire Line
	9200 3150 8800 3150
Wire Wire Line
	8800 3150 8800 3400
Wire Wire Line
	8800 3950 9200 3950
Wire Wire Line
	8800 3700 8800 3950
Connection ~ 9200 3150
Connection ~ 9200 3950
$Comp
L power:+5VA #PWR0199
U 1 1 5D90F69F
P 2050 1150
F 0 "#PWR0199" H 2050 1000 50  0001 C CNN
F 1 "+5VA" H 2050 1300 50  0000 C CNN
F 2 "" H 2050 1150 50  0001 C CNN
F 3 "" H 2050 1150 50  0001 C CNN
	1    2050 1150
	1    0    0    -1  
$EndComp
$Comp
L Relay:FRT5 K?
U 1 1 5C7AD7FD
P 1650 1650
AR Path="/5C4E1326/5C7AD7FD" Ref="K?"  Part="1" 
AR Path="/5C5F098F/5C5F0B20/5C7AD7FD" Ref="K1"  Part="1" 
F 0 "K1" V 1000 1650 50  0000 C CNN
F 1 "EA2-5NU" V 2326 1650 50  0000 C CNN
F 2 "Relay_THT:Relay_DPDT_FRT5" H 2300 1600 50  0001 L CNN
F 3 "http://www.kemet.com/Lists/ProductCatalog/Attachments/514/KEM_R7001_EA2_EB2.pdf" H 2300 1800 50  0001 C CNN
F 4 "Kemet" H -100 -400 50  0001 C CNN "Manufacturer"
F 5 "EA2-5NU" H -100 -400 50  0001 C CNN "PN"
	1    1650 1650
	0    1    1    0   
$EndComp
Wire Wire Line
	2050 1150 2050 1250
Wire Wire Line
	2050 1250 1950 1250
Wire Wire Line
	1250 1250 1350 1250
Wire Wire Line
	2050 2550 2050 1250
Connection ~ 2050 1250
Wire Wire Line
	1250 1250 1250 2550
Connection ~ 1250 2550
Wire Wire Line
	1250 2550 1250 2650
$EndSCHEMATC
