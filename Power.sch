EESchema Schematic File Version 4
LIBS:Capstone-cache
EELAYER 29 0
EELAYER END
$Descr USLetter 11000 8500
encoding utf-8
Sheet 8 8
Title "Capstone"
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text Notes 6300 6950 0    50   ~ 0
(4) Vout = 5.46V; resistor values were chosen to make +5VD slightly higher than GNDA
Text Notes 6300 7250 0    50   ~ 0
(2) GNDD (digital ground) is to be connected directly to -5VA (negative analog supply)
Text Notes 6300 6850 0    50   ~ 0
(5) This is a "floating" reference to be used exclusively for MDAC biasing
Text Notes 6300 7150 0    50   ~ 0
(3) Analog ground reference (GND+5V); the LT1118 is capable of sourcing and sinking ±800mA and does\n    not have stability issues when driving large capacitive loads
Text Notes 6300 6750 0    50   ~ 0
(6) 10k RDAC (digital potentiometer)
Text Notes 6300 6650 0    50   ~ 0
(7) A small (~~10mA) bias current improves the transient performance of the LT1118-5 driving GNDA
Text Notes 6300 6550 0    50   ~ 0
(8) Powered from +5VA regulator in order to maintain slow turn-on time
$Comp
L power:GNDD #PWR?
U 1 1 5CB81D8A
P 1000 4150
AR Path="/5CBF1A8E/5CB81D8A" Ref="#PWR?"  Part="1" 
AR Path="/5E105304/5CB81D8A" Ref="#PWR0158"  Part="1" 
F 0 "#PWR0158" H 1000 3900 50  0001 C CNN
F 1 "GNDD" H 1000 4000 50  0000 C CNN
F 2 "" H 1000 4150 50  0001 C CNN
F 3 "" H 1000 4150 50  0001 C CNN
	1    1000 4150
	1    0    0    -1  
$EndComp
$Comp
L Device:CP1 C?
U 1 1 5CB81F83
P 1000 3800
AR Path="/5CBF1A8E/5CB81F83" Ref="C?"  Part="1" 
AR Path="/5E105304/5CB81F83" Ref="C47"  Part="1" 
F 0 "C47" H 1115 3846 50  0000 L CNN
F 1 "100u" H 1115 3755 50  0000 L CNN
F 2 "Capacitor_Tantalum_SMD:CP_EIA-7343-31_Kemet-D" H 1000 3800 50  0001 C CNN
F 3 "http://datasheets.avx.com/TAJ.pdf" H 1000 3800 50  0001 C CNN
F 4 "TAJD107K020RNJ" H 1000 3800 50  0001 C CNN "PN"
F 5 "AVX" H -150 50  50  0001 C CNN "Manufacturer"
F 6 "10%" H 1000 3800 50  0001 C CNN "Tolerance"
F 7 "Ta" H 1000 3800 50  0001 C CNN "Dielectric"
F 8 "20V" H 1000 3800 50  0001 C CNN "Value2"
	1    1000 3800
	1    0    0    -1  
$EndComp
$Comp
L Device:CP1 C?
U 1 1 5CF2CAAB
P 1500 3800
AR Path="/5CBF1A8E/5CF2CAAB" Ref="C?"  Part="1" 
AR Path="/5E105304/5CF2CAAB" Ref="C48"  Part="1" 
F 0 "C48" H 1615 3846 50  0000 L CNN
F 1 "22u" H 1615 3755 50  0000 L CNN
F 2 "Capacitor_Tantalum_SMD:CP_EIA-7343-31_Kemet-D" H 1500 3800 50  0001 C CNN
F 3 "http://datasheets.avx.com/TAJ.pdf" H 1500 3800 50  0001 C CNN
F 4 "TAJD226K020PNJ" H 1500 3800 50  0001 C CNN "PN"
F 5 "AVX" H -650 50  50  0001 C CNN "Manufacturer"
F 6 "10%" H 1500 3800 50  0001 C CNN "Tolerance"
F 7 "Ta" H 1500 3800 50  0001 C CNN "Dielectric"
F 8 "20V" H 1500 3800 50  0001 C CNN "Value2"
	1    1500 3800
	1    0    0    -1  
$EndComp
Wire Wire Line
	1500 4050 1500 3950
Wire Wire Line
	1500 3650 1500 3550
Wire Wire Line
	1000 4050 1500 4050
Connection ~ 4350 3550
Wire Wire Line
	4350 3950 4350 4050
Wire Wire Line
	4350 3550 4350 3650
$Comp
L power:+5VD #PWR?
U 1 1 5CD4D6A6
P 4350 3450
AR Path="/5CBF1A8E/5CD4D6A6" Ref="#PWR?"  Part="1" 
AR Path="/5E105304/5CD4D6A6" Ref="#PWR0146"  Part="1" 
F 0 "#PWR0146" H 4350 3300 50  0001 C CNN
F 1 "+5VD" H 4350 3600 50  0000 C CNN
F 2 "" H 4350 3450 50  0001 C CNN
F 3 "" H 4350 3450 50  0001 C CNN
	1    4350 3450
	1    0    0    -1  
$EndComp
Wire Wire Line
	4350 3450 4350 3550
Wire Wire Line
	1000 3950 1000 4050
Connection ~ 1000 4050
Wire Wire Line
	1000 4050 1000 4150
Wire Wire Line
	1000 3550 1500 3550
Wire Wire Line
	1000 3550 1000 3650
Connection ~ 1500 3550
Connection ~ 1500 4050
$Comp
L Device:CP1 C?
U 1 1 5CD24DE5
P 2000 3800
AR Path="/5CBF1A8E/5CD24DE5" Ref="C?"  Part="1" 
AR Path="/5E105304/5CD24DE5" Ref="C50"  Part="1" 
F 0 "C50" H 2115 3846 50  0000 L CNN
F 1 "10u" H 2115 3755 50  0000 L CNN
F 2 "Capacitor_Tantalum_SMD:CP_EIA-3216-18_Kemet-A" H 2000 3800 50  0001 C CNN
F 3 "http://datasheets.avx.com/TCJ.pdf" H 2000 3800 50  0001 C CNN
F 4 "TCJA106M025R0150" H 2000 3800 50  0001 C CNN "PN"
F 5 "AVX" H -2600 -1200 50  0001 C CNN "Manufacturer"
F 6 "20%" H 2000 3800 50  0001 C CNN "Tolerance"
F 7 "Ta Polymer" H 2000 3800 50  0001 C CNN "Dielectric"
F 8 "25V" H 2000 3800 50  0001 C CNN "Value2"
	1    2000 3800
	1    0    0    -1  
$EndComp
Wire Wire Line
	2000 3550 2000 3650
Wire Wire Line
	2000 3950 2000 4050
$Comp
L Device:CP1 C?
U 1 1 5CF9EB99
P 4350 3800
AR Path="/5CBF1A8E/5CF9EB99" Ref="C?"  Part="1" 
AR Path="/5E105304/5CF9EB99" Ref="C39"  Part="1" 
F 0 "C39" H 4465 3846 50  0000 L CNN
F 1 "10u" H 4465 3755 50  0000 L CNN
F 2 "Capacitor_Tantalum_SMD:CP_EIA-3216-18_Kemet-A" H 4350 3800 50  0001 C CNN
F 3 "http://datasheets.avx.com/TCJ.pdf" H 4350 3800 50  0001 C CNN
F 4 "TCJA106M025R0150" H 4350 3800 50  0001 C CNN "PN"
F 5 "AVX" H -250 -1200 50  0001 C CNN "Manufacturer"
F 6 "20%" H 4350 3800 50  0001 C CNN "Tolerance"
F 7 "Ta Polymer" H 4350 3800 50  0001 C CNN "Dielectric"
F 8 "25V" H 4350 3800 50  0001 C CNN "Value2"
	1    4350 3800
	1    0    0    -1  
$EndComp
Wire Wire Line
	4750 3550 4350 3550
Wire Wire Line
	4350 4050 4750 4050
Wire Wire Line
	4750 4050 4750 3950
Wire Wire Line
	4750 3650 4750 3550
$Comp
L Device:C C54
U 1 1 5D5228B7
P 4750 3800
F 0 "C54" H 4865 3846 50  0000 L CNN
F 1 "100n" H 4865 3755 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 4788 3650 50  0001 C CNN
F 3 "~" H 4750 3800 50  0001 C CNN
F 4 "Murata" H 4750 3800 50  0001 C CNN "Manufacturer"
F 5 "GRM188R71C104KA01D" H 4750 3800 50  0001 C CNN "PN"
F 6 "X7R" H 4750 3800 50  0001 C CNN "Dielectric"
F 7 "10%" H 4750 3800 50  0001 C CNN "Tolerance"
F 8 "16V" H 4750 3800 50  0001 C CNN "Value2"
	1    4750 3800
	1    0    0    -1  
$EndComp
Text Label 9200 3900 2    50   ~ 0
SDA5
Text Label 9200 3800 2    50   ~ 0
SCL5
Wire Wire Line
	7700 3800 8100 3800
Wire Wire Line
	7700 4200 7800 4200
$Comp
L power:+3V3 #PWR?
U 1 1 5CC562E4
P 8000 3300
AR Path="/5CBF1A8E/5CC562E4" Ref="#PWR?"  Part="1" 
AR Path="/5E105304/5CC562E4" Ref="#PWR0154"  Part="1" 
F 0 "#PWR0154" H 8000 3150 50  0001 C CNN
F 1 "+3V3" H 8000 3450 50  0000 C CNN
F 2 "" H 8000 3300 50  0001 C CNN
F 3 "" H 8000 3300 50  0001 C CNN
	1    8000 3300
	1    0    0    -1  
$EndComp
Text Notes 8050 3350 0    50   ~ 0
3.3 to 5V I2C level shifter
Wire Wire Line
	9650 3400 9650 3450
Connection ~ 9650 3400
Connection ~ 8900 3400
Wire Wire Line
	8900 3400 9650 3400
Wire Wire Line
	8900 3800 9300 3800
Connection ~ 8900 3800
Wire Wire Line
	8600 3900 9300 3900
Connection ~ 8600 3900
Wire Wire Line
	8600 3700 8600 3900
Wire Wire Line
	8600 3900 8600 4200
Wire Wire Line
	9650 3350 9650 3400
Wire Wire Line
	8500 3800 8900 3800
Wire Wire Line
	9650 4250 9650 4350
Wire Wire Line
	8600 3400 8900 3400
Wire Wire Line
	8000 3400 8300 3400
Connection ~ 8000 3400
Wire Wire Line
	8000 3300 8000 3400
Wire Wire Line
	8300 3400 8300 3500
$Comp
L power:+5VD #PWR?
U 1 1 5CC1E135
P 9650 3350
AR Path="/5CBF1A8E/5CC1E135" Ref="#PWR?"  Part="1" 
AR Path="/5E105304/5CC1E135" Ref="#PWR0155"  Part="1" 
F 0 "#PWR0155" H 9650 3200 50  0001 C CNN
F 1 "+5VD" H 9650 3500 50  0000 C CNN
F 2 "" H 9650 3350 50  0001 C CNN
F 3 "" H 9650 3350 50  0001 C CNN
	1    9650 3350
	1    0    0    -1  
$EndComp
Wire Wire Line
	8900 3500 8900 3400
Wire Wire Line
	8600 3500 8600 3400
Wire Wire Line
	8900 3800 8900 3700
$Comp
L Device:R_Small R?
U 1 1 5CBFF1A8
P 8900 3600
AR Path="/5CBF1A8E/5CBFF1A8" Ref="R?"  Part="1" 
AR Path="/5E105304/5CBFF1A8" Ref="R54"  Part="1" 
F 0 "R54" H 8959 3646 50  0000 L CNN
F 1 "2.7k" H 8959 3555 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 8900 3600 50  0001 C CNN
F 3 "~" H 8900 3600 50  0001 C CNN
F 4 "Yageo" H 0   0   50  0001 C CNN "Manufacturer"
F 5 "RC0603JR-072K7" H 0   0   50  0001 C CNN "PN"
F 6 "5%" H 0   0   50  0001 C CNN "Tolerance"
	1    8900 3600
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R?
U 1 1 5CBFEEB3
P 8600 3600
AR Path="/5CBF1A8E/5CBFEEB3" Ref="R?"  Part="1" 
AR Path="/5E105304/5CBFEEB3" Ref="R53"  Part="1" 
F 0 "R53" H 8659 3646 50  0000 L CNN
F 1 "2.7k" H 8659 3555 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 8600 3600 50  0001 C CNN
F 3 "~" H 8600 3600 50  0001 C CNN
F 4 "Yageo" H 0   0   50  0001 C CNN "Manufacturer"
F 5 "RC0603JR-072K7" H 0   0   50  0001 C CNN "PN"
F 6 "5%" H 0   0   50  0001 C CNN "Tolerance"
	1    8600 3600
	1    0    0    -1  
$EndComp
Wire Wire Line
	8200 4200 8600 4200
$Comp
L Transistor_FET:2N7002 Q?
U 1 1 5CBC8F49
P 8000 4100
AR Path="/5CBF1A8E/5CBC8F49" Ref="Q?"  Part="1" 
AR Path="/5E105304/5CBC8F49" Ref="Q7"  Part="1" 
F 0 "Q7" V 7950 3950 50  0000 C CNN
F 1 "BSS138" V 8250 4100 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 8200 4025 50  0001 L CIN
F 3 "https://assets.nexperia.com/documents/data-sheet/BSS138P.pdf" H 8000 4100 50  0001 L CNN
F 4 "Nexperia" H 0   0   50  0001 C CNN "Manufacturer"
F 5 "BSS138P,215" H 0   0   50  0001 C CNN "PN"
	1    8000 4100
	0    1    1    0   
$EndComp
Wire Wire Line
	8000 3900 8000 3400
Text GLabel 7700 4200 0    50   BiDi ~ 0
SDA
Text GLabel 7700 3800 0    50   BiDi ~ 0
SCL
$Comp
L Transistor_FET:2N7002 Q?
U 1 1 5CBC4F41
P 8300 3700
AR Path="/5CBF1A8E/5CBC4F41" Ref="Q?"  Part="1" 
AR Path="/5E105304/5CBC4F41" Ref="Q6"  Part="1" 
F 0 "Q6" V 8250 3550 50  0000 C CNN
F 1 "BSS138" V 8550 3700 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 8500 3625 50  0001 L CIN
F 3 "https://assets.nexperia.com/documents/data-sheet/BSS138P.pdf" H 8300 3700 50  0001 L CNN
F 4 "Nexperia" H 0   0   50  0001 C CNN "Manufacturer"
F 5 "BSS138P,215" H 0   0   50  0001 C CNN "PN"
	1    8300 3700
	0    1    1    0   
$EndComp
$Comp
L power:GNDD #PWR?
U 1 1 5CBBA793
P 9650 4350
AR Path="/5CBF1A8E/5CBBA793" Ref="#PWR?"  Part="1" 
AR Path="/5E105304/5CBBA793" Ref="#PWR0160"  Part="1" 
F 0 "#PWR0160" H 9650 4100 50  0001 C CNN
F 1 "GNDD" H 9650 4200 50  0000 C CNN
F 2 "" H 9650 4350 50  0001 C CNN
F 3 "" H 9650 4350 50  0001 C CNN
	1    9650 4350
	1    0    0    -1  
$EndComp
$Comp
L Capstone:MCP45x1-xxxx_MS_Split U?
U 1 1 5CBA5E6C
P 9650 3800
AR Path="/5CBF1A8E/5CBA5E6C" Ref="U?"  Part="1" 
AR Path="/5E105304/5CBA5E6C" Ref="U41"  Part="1" 
F 0 "U41" H 9900 4100 50  0000 R CNN
F 1 "MCP4561" H 9700 3400 50  0000 L CNN
F 2 "Package_SO:MSOP-8_3x3mm_P0.65mm" H 10750 3300 50  0001 C CNN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/22107B.pdf" H 9600 3850 50  0001 C CNN
F 4 "Microchip" H 0   0   50  0001 C CNN "Manufacturer"
F 5 "MCP4561-103E/MS" H 0   0   50  0001 C CNN "PN"
F 6 "10k" H 0   0   50  0001 C CNN "Value2"
	1    9650 3800
	1    0    0    -1  
$EndComp
$Comp
L power:GNDD #PWR0165
U 1 1 5D628B11
P 4350 4150
F 0 "#PWR0165" H 4350 3900 50  0001 C CNN
F 1 "GNDD" H 4350 4000 50  0000 C CNN
F 2 "" H 4350 4150 50  0001 C CNN
F 3 "" H 4350 4150 50  0001 C CNN
	1    4350 4150
	1    0    0    -1  
$EndComp
Wire Wire Line
	4350 4150 4350 4050
Connection ~ 4350 4050
Connection ~ 2250 2200
$Comp
L power:-5VA #PWR?
U 1 1 5DAC0495
P 1350 2300
AR Path="/5C5F098F/5DAC0495" Ref="#PWR?"  Part="1" 
AR Path="/5CBF1A8E/5DAC0495" Ref="#PWR?"  Part="1" 
AR Path="/5E105304/5DAC0495" Ref="#PWR0149"  Part="1" 
F 0 "#PWR0149" H 1350 2400 50  0001 C CNN
F 1 "-5VA" H 1350 2450 50  0000 C CNN
F 2 "" H 1350 2300 50  0001 C CNN
F 3 "" H 1350 2300 50  0001 C CNN
	1    1350 2300
	-1   0    0    1   
$EndComp
Text Notes 1800 1000 0    50   ~ 0
Analog Power Supplies
Wire Wire Line
	1850 1600 1750 1600
Text Notes 7700 2150 0    50   ~ 0
(5)
Text Notes 7000 1000 0    50   ~ 0
MDAC Supply
Wire Wire Line
	6950 900  6950 1050
Connection ~ 6950 1600
Wire Wire Line
	6950 1600 6950 2200
Connection ~ 6950 2200
Wire Wire Line
	6950 2200 6950 2300
Wire Wire Line
	7850 2550 7850 2650
Wire Wire Line
	7850 2250 7850 2200
Wire Wire Line
	7650 2200 7850 2200
$Comp
L Capstone:MCP45x1-xxxx_MS_Split U?
U 2 1 5CBA5FAB
P 7850 2400
AR Path="/5CBF1A8E/5CBA5FAB" Ref="U?"  Part="2" 
AR Path="/5E105304/5CBA5FAB" Ref="U41"  Part="2" 
F 0 "U41" H 7750 2450 50  0000 R CNN
F 1 "MCP4561" H 7750 2350 50  0001 R CNN
F 2 "Package_SO:MSOP-8_3x3mm_P0.65mm" H 8950 1900 50  0001 C CNN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/22107B.pdf" H 7800 2450 50  0001 C CNN
F 4 "Microchip" H -700 0   50  0001 C CNN "Manufacturer"
F 5 "MCP4561-103E/MS" H -700 0   50  0001 C CNN "PN"
F 6 "10k" H -700 0   50  0001 C CNN "Value2"
	2    7850 2400
	-1   0    0    -1  
$EndComp
$Comp
L Capstone:LT1118xS8 U?
U 1 1 5D2E6225
P 7350 2200
AR Path="/5CBF1A8E/5D2E6225" Ref="U?"  Part="1" 
AR Path="/5E105304/5D2E6225" Ref="U40"  Part="1" 
F 0 "U40" H 7350 2442 50  0000 C CNN
F 1 "LT1118xS8" H 7350 2351 50  0000 C CNN
F 2 "Package_SO:SOIC-8_3.9x4.9mm_P1.27mm" H 7350 2500 50  0001 C CNN
F 3 "https://www.analog.com/media/en/technical-documentation/data-sheets/1118fd.pdf" H 7350 2250 50  0001 C CNN
F 4 "Analog Devices" H -700 0   50  0001 C CNN "Manufacturer"
F 5 "LT1118CS8#PBF" H -700 0   50  0001 C CNN "PN"
	1    7350 2200
	1    0    0    -1  
$EndComp
Text Notes 7950 2500 0    50   ~ 0
(6)
Text Notes 4900 1000 0    50   ~ 0
5V Digital Supply
Text Notes 5800 1700 0    50   ~ 0
(4)
Connection ~ 5250 2300
Connection ~ 5600 2300
Wire Wire Line
	5600 2300 5250 2300
Connection ~ 5600 2000
Wire Wire Line
	5600 2050 5600 2000
Wire Wire Line
	5600 2300 5750 2300
Wire Wire Line
	5600 2250 5600 2300
Wire Wire Line
	5250 2100 5250 2300
$Comp
L Device:R_Small R?
U 1 1 5CCF619F
P 5750 2150
AR Path="/5CBF1A8E/5CCF619F" Ref="R?"  Part="1" 
AR Path="/5E105304/5CCF619F" Ref="R52"  Part="1" 
F 0 "R52" H 5809 2196 50  0000 L CNN
F 1 "68k" H 5809 2105 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 5750 2150 50  0001 C CNN
F 3 "~" H 5750 2150 50  0001 C CNN
F 4 "Yageo" H 5750 2150 50  0001 C CNN "Manufacturer"
F 5 "RC0603FR-0768K" H 5750 2150 50  0001 C CNN "PN"
F 6 "1%" H 5750 2150 50  0001 C CNN "Tolerance"
	1    5750 2150
	1    0    0    -1  
$EndComp
Wire Wire Line
	5750 2000 5750 2050
Connection ~ 5750 2000
Wire Wire Line
	5750 2250 5750 2300
Wire Wire Line
	4950 1700 4850 1700
Wire Wire Line
	4950 1800 4850 1800
Wire Wire Line
	5750 1700 5550 1700
Wire Wire Line
	5600 1900 5550 1900
$Comp
L Device:C_Small C?
U 1 1 5CCF6198
P 5600 2150
AR Path="/5CBF1A8E/5CCF6198" Ref="C?"  Part="1" 
AR Path="/5E105304/5CCF6198" Ref="C43"  Part="1" 
F 0 "C43" H 5508 2104 50  0000 R CNN
F 1 "470p" H 5508 2195 50  0000 R CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 5600 2150 50  0001 C CNN
F 3 "https://www.murata.com/~/media/webrenewal/support/library/catalog/products/capacitor/mlcc/c02e.ashx" H 5600 2150 50  0001 C CNN
F 4 "Murata" H -500 0   50  0001 C CNN "Manufacturer"
F 5 "GRM1885C1H471JA01D" H -500 0   50  0001 C CNN "PN"
F 6 "5%" H -500 0   50  0001 C CNN "Tolerance"
F 7 "C0G" H 5600 2150 50  0001 C CNN "Dielectric"
F 8 "50V" H 5600 2150 50  0001 C CNN "Value2"
	1    5600 2150
	1    0    0    1   
$EndComp
Wire Wire Line
	5600 2000 5600 1900
Wire Wire Line
	5750 2000 5600 2000
Wire Wire Line
	5750 1950 5750 2000
Wire Wire Line
	5750 1750 5750 1700
$Comp
L Device:R_Small R?
U 1 1 5CCF6186
P 5750 1850
AR Path="/5CBF1A8E/5CCF6186" Ref="R?"  Part="1" 
AR Path="/5E105304/5CCF6186" Ref="R49"  Part="1" 
F 0 "R49" H 5809 1896 50  0000 L CNN
F 1 "20k" H 5809 1805 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 5750 1850 50  0001 C CNN
F 3 "~" H 5750 1850 50  0001 C CNN
F 4 "Ohmite" H -500 0   50  0001 C CNN "Manufacturer"
F 5 "APC0603B20K0N" H -500 0   50  0001 C CNN "PN"
F 6 "0.1%" H -500 0   50  0001 C CNN "Tolerance"
	1    5750 1850
	1    0    0    -1  
$EndComp
$Comp
L Regulator_Linear:MIC5205YM5 U?
U 1 1 5CCF4A06
P 5250 1800
AR Path="/5CBF1A8E/5CCF4A06" Ref="U?"  Part="1" 
AR Path="/5E105304/5CCF4A06" Ref="U39"  Part="1" 
F 0 "U39" H 5250 2142 50  0000 C CNN
F 1 "MIC5205YM5" H 5250 2051 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23-5" H 5250 2125 50  0001 C CNN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/20005785A.pdf" H 5250 1800 50  0001 C CNN
F 4 "Microchip" H -500 0   50  0001 C CNN "Manufacturer"
F 5 "MIC5205YM5" H -500 0   50  0001 C CNN "PN"
	1    5250 1800
	1    0    0    -1  
$EndComp
Wire Wire Line
	7050 2300 6950 2300
Wire Wire Line
	6950 2200 7050 2200
Wire Wire Line
	7350 2600 7350 2650
$Comp
L power:GNDD #PWR?
U 1 1 5CB9C51A
P 6950 2700
AR Path="/5CBF1A8E/5CB9C51A" Ref="#PWR?"  Part="1" 
AR Path="/5E105304/5CB9C51A" Ref="#PWR0153"  Part="1" 
F 0 "#PWR0153" H 6950 2450 50  0001 C CNN
F 1 "GNDD" H 6950 2550 50  0000 C CNN
F 2 "" H 6950 2700 50  0001 C CNN
F 3 "" H 6950 2700 50  0001 C CNN
	1    6950 2700
	1    0    0    -1  
$EndComp
Wire Wire Line
	7650 2400 7700 2400
Wire Wire Line
	7850 2650 7350 2650
Connection ~ 7350 2650
Wire Wire Line
	6950 2650 6950 2700
Wire Wire Line
	7550 1600 7450 1600
Connection ~ 7450 1600
Wire Wire Line
	7550 1700 7450 1700
Wire Wire Line
	7450 1700 7450 1600
Connection ~ 7850 2200
$Comp
L Device:R_Small R?
U 1 1 5D3FDB47
P 9250 1750
AR Path="/5CBF1A8E/5D3FDB47" Ref="R?"  Part="1" 
AR Path="/5E105304/5D3FDB47" Ref="R48"  Part="1" 
F 0 "R48" H 9309 1796 50  0000 L CNN
F 1 "33" H 9309 1705 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 9250 1750 50  0001 C CNN
F 3 "~" H 9250 1750 50  0001 C CNN
F 4 "Yageo" H -800 0   50  0001 C CNN "Manufacturer"
F 5 "RC0603JR-0733R" H -800 0   50  0001 C CNN "PN"
F 6 "5%" H -800 0   50  0001 C CNN "Tolerance"
F 7 "100mW" H 9250 1750 50  0001 C CNN "Value2"
	1    9250 1750
	1    0    0    -1  
$EndComp
$Comp
L Device:CP1 C?
U 1 1 5D3F848E
P 8750 1900
AR Path="/5CBF1A8E/5D3F848E" Ref="C?"  Part="1" 
AR Path="/5E105304/5D3F848E" Ref="C36"  Part="1" 
F 0 "C36" H 8865 1946 50  0000 L CNN
F 1 "3.3u" H 8865 1855 50  0000 L CNN
F 2 "Capacitor_Tantalum_SMD:CP_EIA-3216-18_Kemet-A" H 8750 1900 50  0001 C CNN
F 3 "http://datasheets.avx.com/TAJ.pdf" H 8750 1900 50  0001 C CNN
F 4 "TAJA335K016RNJ" H 8750 1900 50  0001 C CNN "PN"
F 5 "AVX" H -900 0   50  0001 C CNN "Manufacturer"
F 6 "10%" H 8750 1900 50  0001 C CNN "Tolerance"
F 7 "Ta" H 8750 1900 50  0001 C CNN "Dielectric"
F 8 "16V" H 8750 1900 50  0001 C CNN "Value2"
	1    8750 1900
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R?
U 1 1 5CA7DAC3
P 8350 1750
AR Path="/5CBF1A8E/5CA7DAC3" Ref="R?"  Part="1" 
AR Path="/5E105304/5CA7DAC3" Ref="R47"  Part="1" 
F 0 "R47" H 8409 1796 50  0000 L CNN
F 1 "120k" H 8409 1705 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 8350 1750 50  0001 C CNN
F 3 "~" H 8350 1750 50  0001 C CNN
F 4 "Yageo" H -900 0   50  0001 C CNN "Manufacturer"
F 5 "RC0603FR-07120K" H -900 0   50  0001 C CNN "PN"
F 6 "1%" H -900 0   50  0001 C CNN "Tolerance"
	1    8350 1750
	1    0    0    -1  
$EndComp
Wire Wire Line
	8350 1650 8350 1600
Wire Wire Line
	7850 2000 7850 2200
$Comp
L Device:C_Small C?
U 1 1 5CA8AB8D
P 8200 2050
AR Path="/5CBF1A8E/5CA8AB8D" Ref="C?"  Part="1" 
AR Path="/5E105304/5CA8AB8D" Ref="C42"  Part="1" 
F 0 "C42" H 8108 2004 50  0000 R CNN
F 1 "470p" H 8108 2095 50  0000 R CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 8200 2050 50  0001 C CNN
F 3 "https://www.murata.com/~/media/webrenewal/support/library/catalog/products/capacitor/mlcc/c02e.ashx" H 8200 2050 50  0001 C CNN
F 4 "Murata" H -900 0   50  0001 C CNN "Manufacturer"
F 5 "GRM1885C1H471JA01D" H -900 0   50  0001 C CNN "PN"
F 6 "5%" H -900 0   50  0001 C CNN "Tolerance"
F 7 "C0G" H 8200 2050 50  0001 C CNN "Dielectric"
F 8 "50V" H 8200 2050 50  0001 C CNN "Value2"
	1    8200 2050
	1    0    0    1   
$EndComp
$Comp
L Device:R_Small R?
U 1 1 5CA80753
P 8350 2050
AR Path="/5CBF1A8E/5CA80753" Ref="R?"  Part="1" 
AR Path="/5E105304/5CA80753" Ref="R51"  Part="1" 
F 0 "R51" H 8409 2096 50  0000 L CNN
F 1 "200k" H 8409 2005 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 8350 2050 50  0001 C CNN
F 3 "~" H 8350 2050 50  0001 C CNN
F 4 "Ohmite" H -900 0   50  0001 C CNN "Manufacturer"
F 5 "APC0603B200KN" H -900 0   50  0001 C CNN "PN"
F 6 "0.1%" H -900 0   50  0001 C CNN "Tolerance"
	1    8350 2050
	1    0    0    -1  
$EndComp
Wire Wire Line
	8200 1800 8150 1800
Wire Wire Line
	8350 1600 8150 1600
Wire Wire Line
	9250 1500 9250 1600
$Comp
L power:+3.3VDAC #PWR?
U 1 1 5CA89216
P 9250 1500
AR Path="/5CBF1A8E/5CA89216" Ref="#PWR?"  Part="1" 
AR Path="/5E105304/5CA89216" Ref="#PWR0147"  Part="1" 
F 0 "#PWR0147" H 9400 1450 50  0001 C CNN
F 1 "+3.3VDAC" H 9250 1650 50  0000 C CNN
F 2 "" H 9250 1500 50  0001 C CNN
F 3 "" H 9250 1500 50  0001 C CNN
	1    9250 1500
	1    0    0    -1  
$EndComp
$Comp
L Device:LED D?
U 1 1 5CDDA5B5
P 9250 2000
AR Path="/5CBF1A8E/5CDDA5B5" Ref="D?"  Part="1" 
AR Path="/5E105304/5CDDA5B5" Ref="D15"  Part="1" 
F 0 "D15" V 9289 1882 50  0000 R CNN
F 1 "GREEN" V 9198 1882 50  0000 R CNN
F 2 "Diode_SMD:D_0805_2012Metric" H 9250 2000 50  0001 C CNN
F 3 "~" H 9250 2000 50  0001 C CNN
	1    9250 2000
	0    -1   -1   0   
$EndComp
Wire Wire Line
	9250 1600 9250 1650
Connection ~ 9250 1600
Wire Wire Line
	8350 1600 8750 1600
Wire Wire Line
	8750 1600 8750 1750
Wire Wire Line
	8750 2050 8750 2200
Connection ~ 8350 1600
Wire Wire Line
	8200 1900 8200 1950
Wire Wire Line
	8200 1900 8200 1800
Wire Wire Line
	8350 1900 8350 1950
Connection ~ 8200 1900
Connection ~ 8350 1900
Wire Wire Line
	8350 1850 8350 1900
Wire Wire Line
	8350 1900 8200 1900
Connection ~ 9250 2200
Wire Wire Line
	9250 2150 9250 2200
Wire Wire Line
	9250 2300 9250 2200
$Comp
L Capstone:GNDDAC #PWR?
U 1 1 5CE6E4B5
P 9250 2300
AR Path="/5CBF1A8E/5CE6E4B5" Ref="#PWR?"  Part="1" 
AR Path="/5E105304/5CE6E4B5" Ref="#PWR0151"  Part="1" 
F 0 "#PWR0151" H 9250 2050 50  0001 C CNN
F 1 "GNDDAC" H 9250 2150 50  0000 C CNN
F 2 "" H 9250 2300 50  0001 C CNN
F 3 "" H 9250 2300 50  0001 C CNN
	1    9250 2300
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 5D3B7089
P 1750 1250
AR Path="/5CBF1A8E/5D3B7089" Ref="R?"  Part="1" 
AR Path="/5E105304/5D3B7089" Ref="R42"  Part="1" 
F 0 "R42" H 1820 1296 50  0000 L CNN
F 1 "100m" H 1820 1205 50  0000 L CNN
F 2 "Resistor_SMD:R_1206_3216Metric" V 1680 1250 50  0001 C CNN
F 3 "https://www.susumu.co.jp/common/pdf/n_catalog_partition09_en.pdf?v=20180406" H 1750 1250 50  0001 C CNN
F 4 "KRL1632E-C-R100-F-T5" H 1750 1250 50  0001 C CNN "PN"
F 5 "Susumu" H 1750 1250 50  0001 C CNN "Manufacturer"
F 6 "1%" H 1750 1250 50  0001 C CNN "Tolerance"
F 7 "750mW" H 1750 1250 50  0001 C CNN "Value2"
	1    1750 1250
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 5D400619
P 4850 1300
AR Path="/5CBF1A8E/5D400619" Ref="R?"  Part="1" 
AR Path="/5E105304/5D400619" Ref="R44"  Part="1" 
F 0 "R44" H 4920 1346 50  0000 L CNN
F 1 "100m" H 4920 1255 50  0000 L CNN
F 2 "Resistor_SMD:R_1206_3216Metric" V 4780 1300 50  0001 C CNN
F 3 "https://www.susumu.co.jp/common/pdf/n_catalog_partition09_en.pdf?v=20180406" H 4850 1300 50  0001 C CNN
F 4 "KRL1632E-C-R100-F-T5" H 4850 1300 50  0001 C CNN "PN"
F 5 "Susumu" H 4850 1300 50  0001 C CNN "Manufacturer"
F 6 "1%" H 4850 1300 50  0001 C CNN "Tolerance"
F 7 "750mW" H 4850 1300 50  0001 C CNN "Value2"
	1    4850 1300
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 5D43D6CE
P 6950 1250
AR Path="/5CBF1A8E/5D43D6CE" Ref="R?"  Part="1" 
AR Path="/5E105304/5D43D6CE" Ref="R43"  Part="1" 
F 0 "R43" H 6880 1204 50  0000 R CNN
F 1 "100m" H 6880 1295 50  0000 R CNN
F 2 "Resistor_SMD:R_1206_3216Metric" V 6880 1250 50  0001 C CNN
F 3 "https://www.susumu.co.jp/common/pdf/n_catalog_partition09_en.pdf?v=20180406" H 6950 1250 50  0001 C CNN
F 4 "KRL1632E-C-R100-F-T5" H 6950 1250 50  0001 C CNN "PN"
F 5 "Susumu" H 6950 1250 50  0001 C CNN "Manufacturer"
F 6 "1%" H 6950 1250 50  0001 C CNN "Tolerance"
F 7 "750mW" H 6950 1250 50  0001 C CNN "Value2"
	1    6950 1250
	1    0    0    1   
$EndComp
Connection ~ 4850 900 
Wire Wire Line
	4850 1800 4850 1700
Connection ~ 4850 1700
Wire Wire Line
	1750 1400 1750 1450
$Comp
L Connector:TestPoint TP?
U 1 1 5D98DFC1
P 1650 1450
AR Path="/5CBF1A8E/5D98DFC1" Ref="TP?"  Part="1" 
AR Path="/5E105304/5D98DFC1" Ref="TP15"  Part="1" 
F 0 "TP15" V 1700 1450 50  0000 C CNN
F 1 "I-5VA" V 1650 1650 50  0000 L CNN
F 2 "TestPoint:TestPoint_Pad_D1.5mm" H 1850 1450 50  0001 C CNN
F 3 "~" H 1850 1450 50  0001 C CNN
	1    1650 1450
	0    -1   -1   0   
$EndComp
Wire Wire Line
	1650 1450 1750 1450
Connection ~ 1750 1450
Wire Wire Line
	1750 1450 1750 1600
$Comp
L Connector:TestPoint TP?
U 1 1 5DA77848
P 6850 1050
AR Path="/5CBF1A8E/5DA77848" Ref="TP?"  Part="1" 
AR Path="/5E105304/5DA77848" Ref="TP12"  Part="1" 
F 0 "TP12" V 6900 1050 50  0000 C CNN
F 1 "I+VDAC" V 6850 1250 50  0000 L CNN
F 2 "TestPoint:TestPoint_Pad_D1.5mm" H 7050 1050 50  0001 C CNN
F 3 "~" H 7050 1050 50  0001 C CNN
	1    6850 1050
	0    -1   -1   0   
$EndComp
Wire Wire Line
	6850 1050 6950 1050
Connection ~ 6950 1050
Wire Wire Line
	6950 1050 6950 1100
Connection ~ 1750 1600
Connection ~ 4850 1800
Connection ~ 4850 2300
Wire Wire Line
	4850 2300 5250 2300
Wire Wire Line
	1350 2200 1350 2300
Wire Wire Line
	2250 2200 1750 2200
Connection ~ 1750 2200
Wire Wire Line
	6950 2300 6950 2400
Wire Wire Line
	6950 2650 7350 2650
Wire Wire Line
	6950 2600 6950 2650
Connection ~ 6950 2300
Connection ~ 6950 2650
$Comp
L Device:CP1_Small C?
U 1 1 5DE5E18E
P 6950 2500
AR Path="/5CBF1A8E/5DE5E18E" Ref="C?"  Part="1" 
AR Path="/5E105304/5DE5E18E" Ref="C46"  Part="1" 
F 0 "C46" H 6850 2550 50  0000 R CNN
F 1 "10u" H 6850 2450 50  0000 R CNN
F 2 "Capacitor_Tantalum_SMD:CP_EIA-3216-18_Kemet-A" H 6950 2500 50  0001 C CNN
F 3 "http://datasheets.avx.com/TCJ.pdf" H 6950 2500 50  0001 C CNN
F 4 "TCJA106M025R0150" H 6950 2500 50  0001 C CNN "PN"
F 5 "AVX" H -700 0   50  0001 C CNN "Manufacturer"
F 6 "20%" H 6950 2500 50  0001 C CNN "Tolerance"
F 7 "Ta Polymer" H 6950 2500 50  0001 C CNN "Dielectric"
F 8 "25V" H 6950 2500 50  0001 C CNN "Value2"
	1    6950 2500
	1    0    0    -1  
$EndComp
Wire Wire Line
	4850 1800 4850 1900
Wire Wire Line
	1750 900  1750 1050
Wire Wire Line
	1750 1050 1750 1100
Connection ~ 1750 1050
$Comp
L Connector:TestPoint TP?
U 1 1 5D98DFBB
P 1650 1050
AR Path="/5CBF1A8E/5D98DFBB" Ref="TP?"  Part="1" 
AR Path="/5E105304/5D98DFBB" Ref="TP10"  Part="1" 
F 0 "TP10" V 1700 1050 50  0000 C CNN
F 1 "I+5VA" V 1650 1250 50  0000 L CNN
F 2 "TestPoint:TestPoint_Pad_D1.5mm" H 1850 1050 50  0001 C CNN
F 3 "~" H 1850 1050 50  0001 C CNN
	1    1650 1050
	0    -1   -1   0   
$EndComp
Wire Wire Line
	1650 1050 1750 1050
$Comp
L power:GNDD #PWR0150
U 1 1 5E4FED8C
P 1750 2300
F 0 "#PWR0150" H 1750 2050 50  0001 C CNN
F 1 "GNDD" H 1750 2150 50  0000 C CNN
F 2 "" H 1750 2300 50  0001 C CNN
F 3 "" H 1750 2300 50  0001 C CNN
	1    1750 2300
	1    0    0    -1  
$EndComp
Text Notes 1800 2300 0    50   ~ 0
(2)
Connection ~ 1750 900 
Wire Wire Line
	4850 900  6950 900 
Wire Wire Line
	4850 1450 4850 1550
Wire Wire Line
	1750 900  4850 900 
Wire Wire Line
	4750 1550 4850 1550
Connection ~ 4850 1550
Wire Wire Line
	4850 1550 4850 1700
Connection ~ 4850 1050
Wire Wire Line
	4850 1050 4850 1150
Wire Wire Line
	4850 900  4850 1050
Wire Wire Line
	4750 1050 4850 1050
$Comp
L Connector:TestPoint TP?
U 1 1 5DA2B392
P 4750 1550
AR Path="/5CBF1A8E/5DA2B392" Ref="TP?"  Part="1" 
AR Path="/5E105304/5DA2B392" Ref="TP17"  Part="1" 
F 0 "TP17" V 4800 1650 50  0000 R CNN
F 1 "I-5VD" V 4750 1750 50  0000 L CNN
F 2 "TestPoint:TestPoint_Pad_D1.5mm" H 4950 1550 50  0001 C CNN
F 3 "~" H 4950 1550 50  0001 C CNN
	1    4750 1550
	0    -1   -1   0   
$EndComp
$Comp
L Device:CP1 C?
U 1 1 5CE8A144
P 8750 2450
AR Path="/5CBF1A8E/5CE8A144" Ref="C?"  Part="1" 
AR Path="/5E105304/5CE8A144" Ref="C45"  Part="1" 
F 0 "C45" H 8865 2496 50  0000 L CNN
F 1 "3.3u" H 8865 2405 50  0000 L CNN
F 2 "Capacitor_Tantalum_SMD:CP_EIA-3216-18_Kemet-A" H 8750 2450 50  0001 C CNN
F 3 "http://datasheets.avx.com/TAJ.pdf" H 8750 2450 50  0001 C CNN
F 4 "TAJA335K016RNJ" H 8750 2450 50  0001 C CNN "PN"
F 5 "AVX" H -900 550 50  0001 C CNN "Manufacturer"
F 6 "10%" H 8750 2450 50  0001 C CNN "Tolerance"
F 7 "Ta" H 8750 2450 50  0001 C CNN "Dielectric"
F 8 "16V" H 8750 2450 50  0001 C CNN "Value2"
	1    8750 2450
	1    0    0    -1  
$EndComp
Wire Wire Line
	8750 2200 8750 2300
Wire Wire Line
	8750 2600 8750 2650
Connection ~ 7850 2650
$Comp
L Device:CP1 C?
U 1 1 5CEA9FC4
P 8350 2450
AR Path="/5CBF1A8E/5CEA9FC4" Ref="C?"  Part="1" 
AR Path="/5E105304/5CEA9FC4" Ref="C44"  Part="1" 
F 0 "C44" H 8465 2496 50  0000 L CNN
F 1 "10u" H 8465 2405 50  0000 L CNN
F 2 "Capacitor_Tantalum_SMD:CP_EIA-3216-18_Kemet-A" H 8350 2450 50  0001 C CNN
F 3 "http://datasheets.avx.com/TCJ.pdf" H 8350 2450 50  0001 C CNN
F 4 "TCJA106M025R0150" H 8350 2450 50  0001 C CNN "PN"
F 5 "AVX" H 3750 -2550 50  0001 C CNN "Manufacturer"
F 6 "20%" H 8350 2450 50  0001 C CNN "Tolerance"
F 7 "Ta Polymer" H 8350 2450 50  0001 C CNN "Dielectric"
F 8 "25V" H 8350 2450 50  0001 C CNN "Value2"
	1    8350 2450
	1    0    0    -1  
$EndComp
Wire Wire Line
	8350 2200 8350 2300
Wire Wire Line
	8350 2600 8350 2650
Wire Wire Line
	1750 2300 1750 2200
Wire Wire Line
	1750 800  1750 900 
$Comp
L power:+12V #PWR?
U 1 1 5CBE6FEB
P 1750 800
AR Path="/5C5F098F/5CBE6FEB" Ref="#PWR?"  Part="1" 
AR Path="/5CBF1A8E/5CBE6FEB" Ref="#PWR?"  Part="1" 
AR Path="/5E105304/5CBE6FEB" Ref="#PWR0144"  Part="1" 
F 0 "#PWR0144" H 1750 650 50  0001 C CNN
F 1 "+12V" H 1750 950 50  0000 C CNN
F 2 "" H 1750 800 50  0001 C CNN
F 3 "" H 1750 800 50  0001 C CNN
	1    1750 800 
	1    0    0    -1  
$EndComp
$Comp
L Capstone:LT1963AxT U36
U 1 1 5CEA8F47
P 2250 1700
F 0 "U36" H 2250 2067 50  0000 C CNN
F 1 "LT1963AxT" H 2250 1976 50  0000 C CNN
F 2 "Package_TO_SOT_THT:TO-220-5_P3.4x3.7mm_StaggerEven_Lead3.8mm_Vertical" H 2250 1250 50  0001 C CNN
F 3 "https://www.analog.com/media/en/technical-documentation/data-sheets/1963fc.pdf" H 2250 1150 50  0001 C CNN
F 4 "Analog Devices" H 2250 1700 50  0001 C CNN "Manufacturer"
F 5 "LT1963AET" H 2250 1700 50  0001 C CNN "PN"
	1    2250 1700
	1    0    0    -1  
$EndComp
Wire Wire Line
	1750 1700 1750 1600
Wire Wire Line
	1850 1700 1750 1700
Connection ~ 2750 1900
Wire Wire Line
	2750 1850 2750 1900
Connection ~ 2750 1600
Wire Wire Line
	2750 1600 2750 1650
Wire Wire Line
	2750 1400 2750 1600
Connection ~ 2750 1400
Connection ~ 2750 2200
Wire Wire Line
	3450 2200 2750 2200
Wire Wire Line
	3450 2100 3450 2200
Wire Wire Line
	2650 1600 2750 1600
Wire Wire Line
	2750 1900 2650 1900
Wire Wire Line
	2750 1900 2750 1950
Wire Wire Line
	2750 2150 2750 2200
Wire Wire Line
	2750 2200 2250 2200
Wire Wire Line
	2750 1300 2750 1400
$Comp
L power:+5VA #PWR?
U 1 1 5DB5870A
P 2750 1300
AR Path="/5C5F098F/5DB5870A" Ref="#PWR?"  Part="1" 
AR Path="/5CBF1A8E/5DB5870A" Ref="#PWR?"  Part="1" 
AR Path="/5E105304/5DB5870A" Ref="#PWR0145"  Part="1" 
F 0 "#PWR0145" H 2750 1150 50  0001 C CNN
F 1 "+5VA" H 2750 1450 50  0000 C CNN
F 2 "" H 2750 1300 50  0001 C CNN
F 3 "" H 2750 1300 50  0001 C CNN
	1    2750 1300
	1    0    0    -1  
$EndComp
$Comp
L Capstone:LT1118xST-5 U?
U 1 1 5D2E58F9
P 3450 1800
AR Path="/5CBF1A8E/5D2E58F9" Ref="U?"  Part="1" 
AR Path="/5E105304/5D2E58F9" Ref="U38"  Part="1" 
F 0 "U38" H 3450 2042 50  0000 C CNN
F 1 "LT1118xST-5" H 3450 1951 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-223-3_TabPin2" H 3450 2025 50  0001 C CNN
F 3 "https://www.analog.com/media/en/technical-documentation/data-sheets/1118fd.pdf" H 3450 1800 50  0001 C CNN
F 4 "Analog Devices" H -500 0   50  0001 C CNN "Manufacturer"
F 5 "LT1118CST-5#PBF" H -500 0   50  0001 C CNN "PN"
	1    3450 1800
	1    0    0    -1  
$EndComp
Text Notes 3050 1900 0    50   ~ 0
(8)
Text Notes 3750 1350 0    50   ~ 0
(7)
$Comp
L Device:LED D?
U 1 1 5CD37897
P 3850 1600
AR Path="/5CBF1A8E/5CD37897" Ref="D?"  Part="1" 
AR Path="/5E105304/5CD37897" Ref="D14"  Part="1" 
F 0 "D14" V 3889 1483 50  0000 R CNN
F 1 "GREEN" V 3798 1483 50  0000 R CNN
F 2 "Diode_SMD:D_0805_2012Metric" H 3850 1600 50  0001 C CNN
F 3 "~" H 3850 1600 50  0001 C CNN
	1    3850 1600
	0    -1   -1   0   
$EndComp
Text Notes 3900 2000 0    50   ~ 0
(3)
$Comp
L power:GNDA #PWR?
U 1 1 5CAE18AD
P 3850 2000
AR Path="/5CBF1A8E/5CAE18AD" Ref="#PWR?"  Part="1" 
AR Path="/5E105304/5CAE18AD" Ref="#PWR0148"  Part="1" 
F 0 "#PWR0148" H 3850 1750 50  0001 C CNN
F 1 "GNDA" H 3850 1850 50  0000 C CNN
F 2 "" H 3850 2000 50  0001 C CNN
F 3 "" H 3850 2000 50  0001 C CNN
	1    3850 2000
	1    0    0    -1  
$EndComp
Wire Wire Line
	3150 1800 3050 1800
Wire Wire Line
	3050 1400 3050 1800
Wire Wire Line
	3850 1400 3850 1450
Wire Wire Line
	3750 1800 3850 1800
Wire Wire Line
	3850 1800 3850 1750
Wire Wire Line
	3850 1800 3850 2000
$Comp
L Device:R R?
U 1 1 5CE04BA8
P 3450 1400
AR Path="/5CBF1A8E/5CE04BA8" Ref="R?"  Part="1" 
AR Path="/5E105304/5CE04BA8" Ref="R45"  Part="1" 
F 0 "R45" V 3350 1400 50  0000 C CNN
F 1 "200" V 3450 1400 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 3380 1400 50  0001 C CNN
F 3 "~" H 3450 1400 50  0001 C CNN
F 4 "Yageo" H -500 0   50  0001 C CNN "Manufacturer"
F 5 "RC0603FR-07200R" H -500 0   50  0001 C CNN "PN"
F 6 "1%" H -500 0   50  0001 C CNN "Tolerance"
	1    3450 1400
	0    1    1    0   
$EndComp
Wire Wire Line
	3050 1400 3300 1400
Wire Wire Line
	3600 1400 3850 1400
Connection ~ 3850 1800
Wire Wire Line
	3050 1400 2750 1400
Connection ~ 3050 1400
Wire Wire Line
	2650 1700 2650 1900
Wire Wire Line
	2250 2000 2250 2200
Wire Wire Line
	1750 1700 1750 1800
Wire Wire Line
	1750 2100 1750 2200
Connection ~ 1750 1700
$Comp
L Device:CP1 C?
U 1 1 5CD46B73
P 1750 1950
AR Path="/5CBF1A8E/5CD46B73" Ref="C?"  Part="1" 
AR Path="/5E105304/5CD46B73" Ref="C38"  Part="1" 
F 0 "C38" H 1865 1996 50  0000 L CNN
F 1 "10u" H 1865 1905 50  0000 L CNN
F 2 "Capacitor_Tantalum_SMD:CP_EIA-3216-18_Kemet-A" H 1750 1950 50  0001 C CNN
F 3 "http://datasheets.avx.com/TCJ.pdf" H 1750 1950 50  0001 C CNN
F 4 "TCJA106M025R0150" H 1750 1950 50  0001 C CNN "PN"
F 5 "AVX" H -2850 -3050 50  0001 C CNN "Manufacturer"
F 6 "20%" H 1750 1950 50  0001 C CNN "Tolerance"
F 7 "Ta Polymer" H 1750 1950 50  0001 C CNN "Dielectric"
F 8 "25V" H 1750 1950 50  0001 C CNN "Value2"
	1    1750 1950
	-1   0    0    -1  
$EndComp
$Comp
L Device:CP1 C?
U 1 1 5CE10A03
P 4850 2050
AR Path="/5CBF1A8E/5CE10A03" Ref="C?"  Part="1" 
AR Path="/5E105304/5CE10A03" Ref="C41"  Part="1" 
F 0 "C41" H 4965 2096 50  0000 L CNN
F 1 "10u" H 4965 2005 50  0000 L CNN
F 2 "Capacitor_Tantalum_SMD:CP_EIA-3216-18_Kemet-A" H 4850 2050 50  0001 C CNN
F 3 "http://datasheets.avx.com/TCJ.pdf" H 4850 2050 50  0001 C CNN
F 4 "TCJA106M025R0150" H 4850 2050 50  0001 C CNN "PN"
F 5 "AVX" H 250 -2950 50  0001 C CNN "Manufacturer"
F 6 "20%" H 4850 2050 50  0001 C CNN "Tolerance"
F 7 "Ta Polymer" H 4850 2050 50  0001 C CNN "Dielectric"
F 8 "25V" H 4850 2050 50  0001 C CNN "Value2"
	1    4850 2050
	-1   0    0    -1  
$EndComp
Wire Wire Line
	4850 2200 4850 2300
Wire Wire Line
	1750 1700 1350 1700
Wire Wire Line
	1350 1700 1350 1800
Wire Wire Line
	1350 2200 1750 2200
Wire Wire Line
	1350 2100 1350 2200
Connection ~ 1350 2200
Text Label 1350 1700 0    50   ~ 0
VAin
$Comp
L power:GNDD #PWR?
U 1 1 5CD2B811
P 4850 2350
AR Path="/5CBF1A8E/5CD2B811" Ref="#PWR?"  Part="1" 
AR Path="/5E105304/5CD2B811" Ref="#PWR0152"  Part="1" 
F 0 "#PWR0152" H 4850 2100 50  0001 C CNN
F 1 "GNDD" H 4850 2200 50  0000 C CNN
F 2 "" H 4850 2350 50  0001 C CNN
F 3 "" H 4850 2350 50  0001 C CNN
	1    4850 2350
	1    0    0    -1  
$EndComp
$Comp
L Connector:TestPoint TP14
U 1 1 5D214C8A
P 9350 2200
F 0 "TP14" V 9304 2388 50  0000 L CNN
F 1 "GNDDAC" V 9395 2388 50  0000 L CNN
F 2 "TestPoint:TestPoint_THTPad_D1.5mm_Drill0.7mm" H 9550 2200 50  0001 C CNN
F 3 "~" H 9550 2200 50  0001 C CNN
	1    9350 2200
	0    1    1    0   
$EndComp
$Comp
L Connector:TestPoint TP26
U 1 1 5D23B906
P 9350 1600
F 0 "TP26" V 9396 1788 50  0000 L CNN
F 1 "VDAC" V 9305 1788 50  0000 L CNN
F 2 "TestPoint:TestPoint_Pad_D1.5mm" H 9550 1600 50  0001 C CNN
F 3 "~" H 9550 1600 50  0001 C CNN
	1    9350 1600
	0    1    -1   0   
$EndComp
Wire Wire Line
	9250 1600 9350 1600
Wire Wire Line
	9350 2200 9250 2200
Connection ~ 8750 2200
Wire Wire Line
	6950 1600 7450 1600
Wire Wire Line
	7850 2200 8200 2200
Wire Wire Line
	8200 2150 8200 2200
Connection ~ 8200 2200
Wire Wire Line
	8200 2200 8350 2200
Wire Wire Line
	8350 2150 8350 2200
Connection ~ 8350 2200
Connection ~ 8350 2650
Wire Wire Line
	8350 2650 7850 2650
Wire Wire Line
	8350 2650 8750 2650
Wire Wire Line
	8350 2200 8750 2200
Wire Wire Line
	8750 1600 9250 1600
Connection ~ 8750 1600
Wire Wire Line
	9250 2200 8750 2200
Connection ~ 6950 1450
Wire Wire Line
	6950 1450 6950 1600
Wire Wire Line
	6950 1400 6950 1450
Wire Wire Line
	6950 1450 6850 1450
$Comp
L Connector:TestPoint TP?
U 1 1 5DA7784E
P 6850 1450
AR Path="/5CBF1A8E/5DA7784E" Ref="TP?"  Part="1" 
AR Path="/5E105304/5DA7784E" Ref="TP16"  Part="1" 
F 0 "TP16" V 6900 1450 50  0000 C CNN
F 1 "I-VDAC" V 6850 1650 50  0000 L CNN
F 2 "TestPoint:TestPoint_Pad_D1.5mm" H 7050 1450 50  0001 C CNN
F 3 "~" H 7050 1450 50  0001 C CNN
	1    6850 1450
	0    -1   -1   0   
$EndComp
Wire Wire Line
	4850 2350 4850 2300
$Comp
L Device:CP1 C?
U 1 1 5D6822DC
P 1350 1950
AR Path="/5CBF1A8E/5D6822DC" Ref="C?"  Part="1" 
AR Path="/5E105304/5D6822DC" Ref="C37"  Part="1" 
F 0 "C37" H 1465 1996 50  0000 L CNN
F 1 "100u" H 1465 1905 50  0000 L CNN
F 2 "Capacitor_Tantalum_SMD:CP_EIA-7343-31_Kemet-D" H 1350 1950 50  0001 C CNN
F 3 "http://datasheets.avx.com/TAJ.pdf" H 1350 1950 50  0001 C CNN
F 4 "TAJD107K020RNJ" H 1350 1950 50  0001 C CNN "PN"
F 5 "AVX" H 200 -1800 50  0001 C CNN "Manufacturer"
F 6 "10%" H 1350 1950 50  0001 C CNN "Tolerance"
F 7 "Ta" H 1350 1950 50  0001 C CNN "Dielectric"
F 8 "20V" H 1350 1950 50  0001 C CNN "Value2"
	1    1350 1950
	-1   0    0    -1  
$EndComp
$Comp
L power:+12V #PWR0166
U 1 1 5CD8B11A
P 1000 3450
F 0 "#PWR0166" H 1000 3300 50  0001 C CNN
F 1 "+12V" H 1000 3600 50  0000 C CNN
F 2 "" H 1000 3450 50  0001 C CNN
F 3 "" H 1000 3450 50  0001 C CNN
	1    1000 3450
	1    0    0    -1  
$EndComp
Wire Wire Line
	1000 3450 1000 3550
Connection ~ 1000 3550
NoConn ~ 10000 3800
Text Notes 10000 3750 0    50   ~ 0
(9)
Text Notes 6300 6450 0    50   ~ 0
(9) This pin includes an internal pullup
Wire Wire Line
	2600 7450 2600 7550
Wire Wire Line
	2600 6650 2600 6750
$Comp
L Device:CP1 C?
U 1 1 5D0B2D72
P 2600 6900
AR Path="/5CBF1A8E/5D0B2D72" Ref="C?"  Part="1" 
AR Path="/5E105304/5D0B2D72" Ref="C4"  Part="1" 
AR Path="/5C5F098F/5C5F0B20/5D0B2D72" Ref="C?"  Part="1" 
F 0 "C4" H 2715 6946 50  0000 L CNN
F 1 "10u" H 2715 6855 50  0000 L CNN
F 2 "Capacitor_Tantalum_SMD:CP_EIA-3216-18_Kemet-A" H 2600 6900 50  0001 C CNN
F 3 "http://datasheets.avx.com/TCJ.pdf" H 2600 6900 50  0001 C CNN
F 4 "TCJA106M025R0150" H 2600 6900 50  0001 C CNN "PN"
F 5 "AVX" H -5000 1900 50  0001 C CNN "Manufacturer"
F 6 "20%" H 2600 6900 50  0001 C CNN "Tolerance"
F 7 "Ta Polymer" H 2600 6900 50  0001 C CNN "Dielectric"
F 8 "25V" H 2600 6900 50  0001 C CNN "Value2"
	1    2600 6900
	1    0    0    -1  
$EndComp
$Comp
L Device:CP1 C?
U 1 1 5D0B2D7B
P 2600 7300
AR Path="/5CBF1A8E/5D0B2D7B" Ref="C?"  Part="1" 
AR Path="/5E105304/5D0B2D7B" Ref="C9"  Part="1" 
AR Path="/5C5F098F/5C5F0B20/5D0B2D7B" Ref="C?"  Part="1" 
F 0 "C9" H 2715 7346 50  0000 L CNN
F 1 "10u" H 2715 7255 50  0000 L CNN
F 2 "Capacitor_Tantalum_SMD:CP_EIA-3216-18_Kemet-A" H 2600 7300 50  0001 C CNN
F 3 "http://datasheets.avx.com/TCJ.pdf" H 2600 7300 50  0001 C CNN
F 4 "TCJA106M025R0150" H 2600 7300 50  0001 C CNN "PN"
F 5 "AVX" H -5000 1800 50  0001 C CNN "Manufacturer"
F 6 "20%" H 2600 7300 50  0001 C CNN "Tolerance"
F 7 "Ta Polymer" H 2600 7300 50  0001 C CNN "Dielectric"
F 8 "25V" H 2600 7300 50  0001 C CNN "Value2"
	1    2600 7300
	1    0    0    -1  
$EndComp
Wire Wire Line
	2600 7050 2600 7100
Connection ~ 2600 7100
Wire Wire Line
	2600 7100 2600 7150
$Comp
L Device:CP1 C?
U 1 1 5D0B2D8F
P 1000 7100
AR Path="/5CBF1A8E/5D0B2D8F" Ref="C?"  Part="1" 
AR Path="/5E105304/5D0B2D8F" Ref="C6"  Part="1" 
AR Path="/5C5F098F/5C5F0B20/5D0B2D8F" Ref="C?"  Part="1" 
F 0 "C6" H 1115 7146 50  0000 L CNN
F 1 "22u" H 1115 7055 50  0000 L CNN
F 2 "Capacitor_Tantalum_SMD:CP_EIA-7343-31_Kemet-D" H 1000 7100 50  0001 C CNN
F 3 "http://datasheets.avx.com/TAJ.pdf" H 1000 7100 50  0001 C CNN
F 4 "TAJD226K020PNJ" H 1000 7100 50  0001 C CNN "PN"
F 5 "AVX" H -3750 3350 50  0001 C CNN "Manufacturer"
F 6 "10%" H 1000 7100 50  0001 C CNN "Tolerance"
F 7 "Ta" H 1000 7100 50  0001 C CNN "Dielectric"
F 8 "20V" H 1000 7100 50  0001 C CNN "Value2"
	1    1000 7100
	1    0    0    -1  
$EndComp
$Comp
L power:-5VA #PWR?
U 1 1 5D0B2DA3
P 1000 7650
AR Path="/5CBF1A8E/5D0B2DA3" Ref="#PWR?"  Part="1" 
AR Path="/5E105304/5D0B2DA3" Ref="#PWR0170"  Part="1" 
AR Path="/5C5F098F/5C5F0B20/5D0B2DA3" Ref="#PWR?"  Part="1" 
F 0 "#PWR0170" H 1000 7750 50  0001 C CNN
F 1 "-5VA" H 1000 7800 50  0000 C CNN
F 2 "" H 1000 7650 50  0001 C CNN
F 3 "" H 1000 7650 50  0001 C CNN
	1    1000 7650
	-1   0    0    1   
$EndComp
Wire Wire Line
	2600 7100 3000 7100
Wire Wire Line
	3000 7100 3000 7200
$Comp
L power:GNDA #PWR0171
U 1 1 5D12CDA8
P 1800 7200
F 0 "#PWR0171" H 1800 6950 50  0001 C CNN
F 1 "GNDA" H 1800 7050 50  0000 C CNN
F 2 "" H 1800 7200 50  0001 C CNN
F 3 "" H 1800 7200 50  0001 C CNN
	1    1800 7200
	1    0    0    -1  
$EndComp
$Comp
L power:+5VA #PWR?
U 1 1 5D0B2D9D
P 1000 6550
AR Path="/5CBF1A8E/5D0B2D9D" Ref="#PWR?"  Part="1" 
AR Path="/5E105304/5D0B2D9D" Ref="#PWR0156"  Part="1" 
AR Path="/5C5F098F/5C5F0B20/5D0B2D9D" Ref="#PWR?"  Part="1" 
F 0 "#PWR0156" H 1000 6400 50  0001 C CNN
F 1 "+5VA" H 1000 6700 50  0000 C CNN
F 2 "" H 1000 6550 50  0001 C CNN
F 3 "" H 1000 6550 50  0001 C CNN
	1    1000 6550
	1    0    0    -1  
$EndComp
Connection ~ 1000 7550
Connection ~ 1000 6650
Wire Wire Line
	1000 6550 1000 6650
Wire Wire Line
	1000 6950 1000 6650
Wire Wire Line
	1000 7650 1000 7550
Wire Wire Line
	1000 7550 1000 7250
$Comp
L Device:CP1 C?
U 1 1 5D0B2D4D
P 2200 7100
AR Path="/5CBF1A8E/5D0B2D4D" Ref="C?"  Part="1" 
AR Path="/5E105304/5D0B2D4D" Ref="C7"  Part="1" 
AR Path="/5C5F098F/5C5F0B20/5D0B2D4D" Ref="C?"  Part="1" 
F 0 "C7" H 2315 7146 50  0000 L CNN
F 1 "22u" H 2315 7055 50  0000 L CNN
F 2 "Capacitor_Tantalum_SMD:CP_EIA-7343-31_Kemet-D" H 2200 7100 50  0001 C CNN
F 3 "http://datasheets.avx.com/TAJ.pdf" H 2200 7100 50  0001 C CNN
F 4 "TAJD226K020PNJ" H 2200 7100 50  0001 C CNN "PN"
F 5 "AVX" H -2550 3350 50  0001 C CNN "Manufacturer"
F 6 "10%" H 2200 7100 50  0001 C CNN "Tolerance"
F 7 "Ta" H 2200 7100 50  0001 C CNN "Dielectric"
F 8 "20V" H 2200 7100 50  0001 C CNN "Value2"
	1    2200 7100
	1    0    0    -1  
$EndComp
Wire Wire Line
	1400 7100 1800 7100
Wire Wire Line
	1400 7050 1400 7100
Connection ~ 1400 7100
$Comp
L Device:CP1 C?
U 1 1 5D0B2D65
P 1400 7300
AR Path="/5CBF1A8E/5D0B2D65" Ref="C?"  Part="1" 
AR Path="/5E105304/5D0B2D65" Ref="C8"  Part="1" 
AR Path="/5C5F098F/5C5F0B20/5D0B2D65" Ref="C?"  Part="1" 
F 0 "C8" H 1515 7346 50  0000 L CNN
F 1 "10u" H 1515 7255 50  0000 L CNN
F 2 "Capacitor_Tantalum_SMD:CP_EIA-3216-18_Kemet-A" H 1400 7300 50  0001 C CNN
F 3 "http://datasheets.avx.com/TCJ.pdf" H 1400 7300 50  0001 C CNN
F 4 "TCJA106M025R0150" H 1400 7300 50  0001 C CNN "PN"
F 5 "AVX" H -6200 1800 50  0001 C CNN "Manufacturer"
F 6 "20%" H 1400 7300 50  0001 C CNN "Tolerance"
F 7 "Ta Polymer" H 1400 7300 50  0001 C CNN "Dielectric"
F 8 "25V" H 1400 7300 50  0001 C CNN "Value2"
	1    1400 7300
	1    0    0    -1  
$EndComp
$Comp
L Device:CP1 C?
U 1 1 5D0B2D5C
P 1400 6900
AR Path="/5CBF1A8E/5D0B2D5C" Ref="C?"  Part="1" 
AR Path="/5E105304/5D0B2D5C" Ref="C3"  Part="1" 
AR Path="/5C5F098F/5C5F0B20/5D0B2D5C" Ref="C?"  Part="1" 
F 0 "C3" H 1515 6946 50  0000 L CNN
F 1 "10u" H 1515 6855 50  0000 L CNN
F 2 "Capacitor_Tantalum_SMD:CP_EIA-3216-18_Kemet-A" H 1400 6900 50  0001 C CNN
F 3 "http://datasheets.avx.com/TCJ.pdf" H 1400 6900 50  0001 C CNN
F 4 "TCJA106M025R0150" H 1400 6900 50  0001 C CNN "PN"
F 5 "AVX" H -6200 1900 50  0001 C CNN "Manufacturer"
F 6 "20%" H 1400 6900 50  0001 C CNN "Tolerance"
F 7 "Ta Polymer" H 1400 6900 50  0001 C CNN "Dielectric"
F 8 "25V" H 1400 6900 50  0001 C CNN "Value2"
	1    1400 6900
	1    0    0    -1  
$EndComp
Wire Wire Line
	1400 6650 1400 6750
Wire Wire Line
	1400 7450 1400 7550
Wire Wire Line
	1400 7100 1400 7150
Wire Wire Line
	1800 7100 1800 7200
Wire Wire Line
	2200 6650 2200 6950
Wire Wire Line
	2200 7250 2200 7550
$Comp
L power:GNDA #PWR0174
U 1 1 5D2B2785
P 3000 7200
F 0 "#PWR0174" H 3000 6950 50  0001 C CNN
F 1 "GNDA" H 3000 7050 50  0000 C CNN
F 2 "" H 3000 7200 50  0001 C CNN
F 3 "" H 3000 7200 50  0001 C CNN
	1    3000 7200
	1    0    0    -1  
$EndComp
$Comp
L power:+5VA #PWR?
U 1 1 5D2B2E93
P 2200 6550
AR Path="/5CBF1A8E/5D2B2E93" Ref="#PWR?"  Part="1" 
AR Path="/5E105304/5D2B2E93" Ref="#PWR0175"  Part="1" 
AR Path="/5C5F098F/5C5F0B20/5D2B2E93" Ref="#PWR?"  Part="1" 
F 0 "#PWR0175" H 2200 6400 50  0001 C CNN
F 1 "+5VA" H 2200 6700 50  0000 C CNN
F 2 "" H 2200 6550 50  0001 C CNN
F 3 "" H 2200 6550 50  0001 C CNN
	1    2200 6550
	1    0    0    -1  
$EndComp
Wire Wire Line
	2200 6550 2200 6650
Connection ~ 2200 6650
$Comp
L power:-5VA #PWR?
U 1 1 5D2D9856
P 2200 7650
AR Path="/5CBF1A8E/5D2D9856" Ref="#PWR?"  Part="1" 
AR Path="/5E105304/5D2D9856" Ref="#PWR0176"  Part="1" 
AR Path="/5C5F098F/5C5F0B20/5D2D9856" Ref="#PWR?"  Part="1" 
F 0 "#PWR0176" H 2200 7750 50  0001 C CNN
F 1 "-5VA" H 2200 7800 50  0000 C CNN
F 2 "" H 2200 7650 50  0001 C CNN
F 3 "" H 2200 7650 50  0001 C CNN
	1    2200 7650
	-1   0    0    1   
$EndComp
Wire Wire Line
	2200 7650 2200 7550
Connection ~ 2200 7550
Wire Wire Line
	1000 6650 1400 6650
Wire Wire Line
	1000 7550 1400 7550
Wire Wire Line
	2200 6650 2600 6650
Wire Wire Line
	2200 7550 2600 7550
Wire Wire Line
	3800 7450 3800 7550
Wire Wire Line
	3800 6650 3800 6750
$Comp
L Device:CP1 C?
U 1 1 5D3786C0
P 3800 6900
AR Path="/5CBF1A8E/5D3786C0" Ref="C?"  Part="1" 
AR Path="/5E105304/5D3786C0" Ref="C28"  Part="1" 
AR Path="/5C5F098F/5C5F0B20/5D3786C0" Ref="C?"  Part="1" 
F 0 "C28" H 3915 6946 50  0000 L CNN
F 1 "10u" H 3915 6855 50  0000 L CNN
F 2 "Capacitor_Tantalum_SMD:CP_EIA-3216-18_Kemet-A" H 3800 6900 50  0001 C CNN
F 3 "http://datasheets.avx.com/TCJ.pdf" H 3800 6900 50  0001 C CNN
F 4 "TCJA106M025R0150" H 3800 6900 50  0001 C CNN "PN"
F 5 "AVX" H -3800 1900 50  0001 C CNN "Manufacturer"
F 6 "20%" H 3800 6900 50  0001 C CNN "Tolerance"
F 7 "Ta Polymer" H 3800 6900 50  0001 C CNN "Dielectric"
F 8 "25V" H 3800 6900 50  0001 C CNN "Value2"
	1    3800 6900
	1    0    0    -1  
$EndComp
$Comp
L Device:CP1 C?
U 1 1 5D3786C9
P 3800 7300
AR Path="/5CBF1A8E/5D3786C9" Ref="C?"  Part="1" 
AR Path="/5E105304/5D3786C9" Ref="C30"  Part="1" 
AR Path="/5C5F098F/5C5F0B20/5D3786C9" Ref="C?"  Part="1" 
F 0 "C30" H 3915 7346 50  0000 L CNN
F 1 "10u" H 3915 7255 50  0000 L CNN
F 2 "Capacitor_Tantalum_SMD:CP_EIA-3216-18_Kemet-A" H 3800 7300 50  0001 C CNN
F 3 "http://datasheets.avx.com/TCJ.pdf" H 3800 7300 50  0001 C CNN
F 4 "TCJA106M025R0150" H 3800 7300 50  0001 C CNN "PN"
F 5 "AVX" H -3800 1800 50  0001 C CNN "Manufacturer"
F 6 "20%" H 3800 7300 50  0001 C CNN "Tolerance"
F 7 "Ta Polymer" H 3800 7300 50  0001 C CNN "Dielectric"
F 8 "25V" H 3800 7300 50  0001 C CNN "Value2"
	1    3800 7300
	1    0    0    -1  
$EndComp
Wire Wire Line
	3800 7050 3800 7100
Connection ~ 3800 7100
Wire Wire Line
	3800 7100 3800 7150
Wire Wire Line
	3800 7100 4200 7100
Wire Wire Line
	4200 7100 4200 7200
$Comp
L Device:CP1 C?
U 1 1 5D3786D7
P 3400 7100
AR Path="/5CBF1A8E/5D3786D7" Ref="C?"  Part="1" 
AR Path="/5E105304/5D3786D7" Ref="C29"  Part="1" 
AR Path="/5C5F098F/5C5F0B20/5D3786D7" Ref="C?"  Part="1" 
F 0 "C29" H 3515 7146 50  0000 L CNN
F 1 "22u" H 3515 7055 50  0000 L CNN
F 2 "Capacitor_Tantalum_SMD:CP_EIA-7343-31_Kemet-D" H 3400 7100 50  0001 C CNN
F 3 "http://datasheets.avx.com/TAJ.pdf" H 3400 7100 50  0001 C CNN
F 4 "TAJD226K020PNJ" H 3400 7100 50  0001 C CNN "PN"
F 5 "AVX" H -1350 3350 50  0001 C CNN "Manufacturer"
F 6 "10%" H 3400 7100 50  0001 C CNN "Tolerance"
F 7 "Ta" H 3400 7100 50  0001 C CNN "Dielectric"
F 8 "20V" H 3400 7100 50  0001 C CNN "Value2"
	1    3400 7100
	1    0    0    -1  
$EndComp
Wire Wire Line
	3400 6650 3400 6950
Wire Wire Line
	3400 7250 3400 7550
$Comp
L power:GNDA #PWR0106
U 1 1 5D3786DF
P 4200 7200
F 0 "#PWR0106" H 4200 6950 50  0001 C CNN
F 1 "GNDA" H 4200 7050 50  0000 C CNN
F 2 "" H 4200 7200 50  0001 C CNN
F 3 "" H 4200 7200 50  0001 C CNN
	1    4200 7200
	1    0    0    -1  
$EndComp
$Comp
L power:+5VA #PWR?
U 1 1 5D3786E5
P 3400 6550
AR Path="/5CBF1A8E/5D3786E5" Ref="#PWR?"  Part="1" 
AR Path="/5E105304/5D3786E5" Ref="#PWR0104"  Part="1" 
AR Path="/5C5F098F/5C5F0B20/5D3786E5" Ref="#PWR?"  Part="1" 
F 0 "#PWR0104" H 3400 6400 50  0001 C CNN
F 1 "+5VA" H 3400 6700 50  0000 C CNN
F 2 "" H 3400 6550 50  0001 C CNN
F 3 "" H 3400 6550 50  0001 C CNN
	1    3400 6550
	1    0    0    -1  
$EndComp
Wire Wire Line
	3400 6550 3400 6650
Connection ~ 3400 6650
$Comp
L power:-5VA #PWR?
U 1 1 5D3786ED
P 3400 7650
AR Path="/5CBF1A8E/5D3786ED" Ref="#PWR?"  Part="1" 
AR Path="/5E105304/5D3786ED" Ref="#PWR0108"  Part="1" 
AR Path="/5C5F098F/5C5F0B20/5D3786ED" Ref="#PWR?"  Part="1" 
F 0 "#PWR0108" H 3400 7750 50  0001 C CNN
F 1 "-5VA" H 3400 7800 50  0000 C CNN
F 2 "" H 3400 7650 50  0001 C CNN
F 3 "" H 3400 7650 50  0001 C CNN
	1    3400 7650
	-1   0    0    1   
$EndComp
Wire Wire Line
	3400 7650 3400 7550
Connection ~ 3400 7550
Wire Wire Line
	3400 6650 3800 6650
Wire Wire Line
	3400 7550 3800 7550
Wire Wire Line
	1500 4050 2000 4050
Wire Wire Line
	1500 3550 2000 3550
$Comp
L Connector:TestPoint TP?
U 1 1 5DA2B38C
P 4750 1050
AR Path="/5CBF1A8E/5DA2B38C" Ref="TP?"  Part="1" 
AR Path="/5E105304/5DA2B38C" Ref="TP28"  Part="1" 
F 0 "TP28" V 4800 1150 50  0000 R CNN
F 1 "I+5VD" V 4750 1250 50  0000 L CNN
F 2 "TestPoint:TestPoint_Pad_D1.5mm" H 4950 1050 50  0001 C CNN
F 3 "~" H 4950 1050 50  0001 C CNN
	1    4750 1050
	0    -1   -1   0   
$EndComp
Wire Wire Line
	5750 1700 5750 1600
Connection ~ 5750 1700
$Comp
L power:+5VD #PWR0161
U 1 1 5CF52778
P 5750 1600
F 0 "#PWR0161" H 5750 1450 50  0001 C CNN
F 1 "+5VD" H 5750 1750 50  0000 C CNN
F 2 "" H 5750 1600 50  0001 C CNN
F 3 "" H 5750 1600 50  0001 C CNN
	1    5750 1600
	1    0    0    -1  
$EndComp
$Comp
L Regulator_Linear:MIC5205YM5 U?
U 1 1 5CA7107C
P 7850 1700
AR Path="/5CBF1A8E/5CA7107C" Ref="U?"  Part="1" 
AR Path="/5E105304/5CA7107C" Ref="U37"  Part="1" 
F 0 "U37" H 7850 2042 50  0000 C CNN
F 1 "MIC5205YM5" H 7850 1951 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23-5" H 7850 2025 50  0001 C CNN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/20005785A.pdf" H 7850 1700 50  0001 C CNN
F 4 "Microchip" H -900 0   50  0001 C CNN "Manufacturer"
F 5 "MIC5205YM5" H -900 0   50  0001 C CNN "PN"
	1    7850 1700
	1    0    0    -1  
$EndComp
$Comp
L power:+3V3 #PWR0178
U 1 1 5CE0ECF8
P 2650 3450
F 0 "#PWR0178" H 2650 3300 50  0001 C CNN
F 1 "+3V3" H 2650 3600 50  0000 C CNN
F 2 "" H 2650 3450 50  0001 C CNN
F 3 "" H 2650 3450 50  0001 C CNN
	1    2650 3450
	1    0    0    -1  
$EndComp
$Comp
L power:GNDD #PWR?
U 1 1 5CE0F06C
P 2650 4150
AR Path="/5CBF1A8E/5CE0F06C" Ref="#PWR?"  Part="1" 
AR Path="/5E105304/5CE0F06C" Ref="#PWR0179"  Part="1" 
F 0 "#PWR0179" H 2650 3900 50  0001 C CNN
F 1 "GNDD" H 2650 4000 50  0000 C CNN
F 2 "" H 2650 4150 50  0001 C CNN
F 3 "" H 2650 4150 50  0001 C CNN
	1    2650 4150
	1    0    0    -1  
$EndComp
Wire Wire Line
	2650 3450 2650 3550
Wire Wire Line
	2650 3950 2650 4050
Wire Wire Line
	1000 5050 1000 5350
Connection ~ 1000 5050
Wire Wire Line
	1500 5050 1000 5050
Wire Wire Line
	1000 5950 1000 6050
Connection ~ 1000 5950
Wire Wire Line
	1500 5950 1000 5950
Wire Wire Line
	1000 5650 1000 5950
Wire Wire Line
	1000 4950 1000 5050
Wire Wire Line
	1500 5050 1500 5150
Wire Wire Line
	1500 5850 1500 5950
$Comp
L Device:CP1 C?
U 1 1 5D252606
P 1000 5500
AR Path="/5CBF1A8E/5D252606" Ref="C?"  Part="1" 
AR Path="/5E105304/5D252606" Ref="C51"  Part="1" 
F 0 "C51" H 1115 5546 50  0000 L CNN
F 1 "100u" H 1115 5455 50  0000 L CNN
F 2 "Capacitor_Tantalum_SMD:CP_EIA-7343-31_Kemet-D" H 1000 5500 50  0001 C CNN
F 3 "http://datasheets.avx.com/TAJ.pdf" H 1000 5500 50  0001 C CNN
F 4 "TAJD107K020RNJ" H 1000 5500 50  0001 C CNN "PN"
F 5 "AVX" H -2250 1750 50  0001 C CNN "Manufacturer"
F 6 "10%" H 1000 5500 50  0001 C CNN "Tolerance"
F 7 "Ta" H 1000 5500 50  0001 C CNN "Dielectric"
F 8 "20V" H 1000 5500 50  0001 C CNN "Value2"
	1    1000 5500
	1    0    0    -1  
$EndComp
$Comp
L power:+5VA #PWR?
U 1 1 5D25260E
P 1000 4950
AR Path="/5CBF1A8E/5D25260E" Ref="#PWR?"  Part="1" 
AR Path="/5E105304/5D25260E" Ref="#PWR0157"  Part="1" 
F 0 "#PWR0157" H 1000 4800 50  0001 C CNN
F 1 "+5VA" H 1000 5100 50  0000 C CNN
F 2 "" H 1000 4950 50  0001 C CNN
F 3 "" H 1000 4950 50  0001 C CNN
	1    1000 4950
	1    0    0    -1  
$EndComp
$Comp
L power:-5VA #PWR?
U 1 1 5D2A82BE
P 1000 6050
AR Path="/5CBF1A8E/5D2A82BE" Ref="#PWR?"  Part="1" 
AR Path="/5E105304/5D2A82BE" Ref="#PWR0159"  Part="1" 
F 0 "#PWR0159" H 1000 6150 50  0001 C CNN
F 1 "-5VA" H 1000 6200 50  0000 C CNN
F 2 "" H 1000 6050 50  0001 C CNN
F 3 "" H 1000 6050 50  0001 C CNN
	1    1000 6050
	-1   0    0    1   
$EndComp
$Comp
L Device:CP1 C?
U 1 1 5CB68DA2
P 1500 5700
AR Path="/5CBF1A8E/5CB68DA2" Ref="C?"  Part="1" 
AR Path="/5E105304/5CB68DA2" Ref="C53"  Part="1" 
F 0 "C53" H 1615 5746 50  0000 L CNN
F 1 "100u" H 1615 5655 50  0000 L CNN
F 2 "Capacitor_Tantalum_SMD:CP_EIA-7343-31_Kemet-D" H 1500 5700 50  0001 C CNN
F 3 "http://datasheets.avx.com/TAJ.pdf" H 1500 5700 50  0001 C CNN
F 4 "TAJD107K020RNJ" H 1500 5700 50  0001 C CNN "PN"
F 5 "AVX" H 350 200 50  0001 C CNN "Manufacturer"
F 6 "10%" H 1500 5700 50  0001 C CNN "Tolerance"
F 7 "Ta" H 1500 5700 50  0001 C CNN "Dielectric"
F 8 "20V" H 1500 5700 50  0001 C CNN "Value2"
	1    1500 5700
	1    0    0    -1  
$EndComp
$Comp
L Device:CP1 C?
U 1 1 5CB686F1
P 1500 5300
AR Path="/5CBF1A8E/5CB686F1" Ref="C?"  Part="1" 
AR Path="/5E105304/5CB686F1" Ref="C52"  Part="1" 
F 0 "C52" H 1615 5346 50  0000 L CNN
F 1 "100u" H 1615 5255 50  0000 L CNN
F 2 "Capacitor_Tantalum_SMD:CP_EIA-7343-31_Kemet-D" H 1500 5300 50  0001 C CNN
F 3 "http://datasheets.avx.com/TAJ.pdf" H 1500 5300 50  0001 C CNN
F 4 "TAJD107K020RNJ" H 1500 5300 50  0001 C CNN "PN"
F 5 "AVX" H 350 300 50  0001 C CNN "Manufacturer"
F 6 "10%" H 1500 5300 50  0001 C CNN "Tolerance"
F 7 "Ta" H 1500 5300 50  0001 C CNN "Dielectric"
F 8 "20V" H 1500 5300 50  0001 C CNN "Value2"
	1    1500 5300
	1    0    0    -1  
$EndComp
$Comp
L power:GNDA #PWR?
U 1 1 5CF0B2A1
P 2000 5600
AR Path="/5CBF1A8E/5CF0B2A1" Ref="#PWR?"  Part="1" 
AR Path="/5E105304/5CF0B2A1" Ref="#PWR0162"  Part="1" 
F 0 "#PWR0162" H 2000 5350 50  0001 C CNN
F 1 "GNDA" H 2000 5450 50  0000 C CNN
F 2 "" H 2000 5600 50  0001 C CNN
F 3 "" H 2000 5600 50  0001 C CNN
	1    2000 5600
	-1   0    0    -1  
$EndComp
Wire Wire Line
	1500 5500 1500 5550
Connection ~ 1500 5500
Wire Wire Line
	2000 5500 2000 5600
Wire Wire Line
	1500 5500 2000 5500
Wire Wire Line
	1500 5450 1500 5500
$Comp
L power:+5VA #PWR?
U 1 1 5D1A81E3
P 2700 5150
AR Path="/5CBF1A8E/5D1A81E3" Ref="#PWR?"  Part="1" 
AR Path="/5E105304/5D1A81E3" Ref="#PWR0186"  Part="1" 
F 0 "#PWR0186" H 2700 5000 50  0001 C CNN
F 1 "+5VA" H 2700 5300 50  0000 C CNN
F 2 "" H 2700 5150 50  0001 C CNN
F 3 "" H 2700 5150 50  0001 C CNN
	1    2700 5150
	1    0    0    -1  
$EndComp
$Comp
L power:-5VA #PWR?
U 1 1 5D1A94AC
P 2700 5850
AR Path="/5CBF1A8E/5D1A94AC" Ref="#PWR?"  Part="1" 
AR Path="/5E105304/5D1A94AC" Ref="#PWR0187"  Part="1" 
F 0 "#PWR0187" H 2700 5950 50  0001 C CNN
F 1 "-5VA" H 2700 6000 50  0000 C CNN
F 2 "" H 2700 5850 50  0001 C CNN
F 3 "" H 2700 5850 50  0001 C CNN
	1    2700 5850
	-1   0    0    1   
$EndComp
Wire Wire Line
	2700 5850 2700 5750
Wire Wire Line
	2700 5350 2700 5250
Wire Wire Line
	3200 5350 3200 5250
Wire Wire Line
	3200 5250 2700 5250
Connection ~ 2700 5250
Wire Wire Line
	2700 5250 2700 5150
Wire Wire Line
	3200 5650 3200 5750
Wire Wire Line
	3200 5750 2700 5750
Connection ~ 2700 5750
Wire Wire Line
	2700 5750 2700 5650
$Comp
L Device:CP1 C?
U 1 1 5D242EA5
P 4200 5500
AR Path="/5CBF1A8E/5D242EA5" Ref="C?"  Part="1" 
AR Path="/5E105304/5D242EA5" Ref="C106"  Part="1" 
AR Path="/5C5F098F/5C5F0B20/5D242EA5" Ref="C?"  Part="1" 
F 0 "C106" H 4315 5546 50  0000 L CNN
F 1 "10u" H 4315 5455 50  0000 L CNN
F 2 "Capacitor_Tantalum_SMD:CP_EIA-3216-18_Kemet-A" H 4200 5500 50  0001 C CNN
F 3 "http://datasheets.avx.com/TCJ.pdf" H 4200 5500 50  0001 C CNN
F 4 "TCJA106M025R0150" H 4200 5500 50  0001 C CNN "PN"
F 5 "AVX" H -3400 500 50  0001 C CNN "Manufacturer"
F 6 "20%" H 4200 5500 50  0001 C CNN "Tolerance"
F 7 "Ta Polymer" H 4200 5500 50  0001 C CNN "Dielectric"
F 8 "25V" H 4200 5500 50  0001 C CNN "Value2"
	1    4200 5500
	1    0    0    -1  
$EndComp
Wire Wire Line
	3200 5250 3700 5250
Wire Wire Line
	4200 5250 4200 5350
Wire Wire Line
	3700 5750 3200 5750
Wire Wire Line
	4200 5650 4200 5750
Connection ~ 3200 5250
Connection ~ 3200 5750
Wire Wire Line
	3700 5250 3700 5350
Wire Wire Line
	3700 5650 3700 5750
Wire Wire Line
	3700 5750 4200 5750
Connection ~ 3700 5750
Wire Wire Line
	4200 5250 3700 5250
Connection ~ 3700 5250
$Comp
L Device:CP1 C?
U 1 1 5D365BD5
P 3200 5500
AR Path="/5CBF1A8E/5D365BD5" Ref="C?"  Part="1" 
AR Path="/5E105304/5D365BD5" Ref="C105"  Part="1" 
AR Path="/5C5F098F/5C5F0B20/5D365BD5" Ref="C?"  Part="1" 
F 0 "C105" H 3315 5546 50  0000 L CNN
F 1 "22u" H 3315 5455 50  0000 L CNN
F 2 "Capacitor_Tantalum_SMD:CP_EIA-7343-31_Kemet-D" H 3200 5500 50  0001 C CNN
F 3 "http://datasheets.avx.com/TAJ.pdf" H 3200 5500 50  0001 C CNN
F 4 "TAJD226K020PNJ" H 3200 5500 50  0001 C CNN "PN"
F 5 "AVX" H -1550 1750 50  0001 C CNN "Manufacturer"
F 6 "10%" H 3200 5500 50  0001 C CNN "Tolerance"
F 7 "Ta" H 3200 5500 50  0001 C CNN "Dielectric"
F 8 "20V" H 3200 5500 50  0001 C CNN "Value2"
	1    3200 5500
	1    0    0    -1  
$EndComp
$Comp
L Connector:TestPoint TP29
U 1 1 5D7F822F
P 5850 3850
F 0 "TP29" H 5792 3876 50  0000 R CNN
F 1 "+5VA" H 5792 3967 50  0000 R CNN
F 2 "TestPoint:TestPoint_Pad_D1.5mm" H 6050 3850 50  0001 C CNN
F 3 "~" H 6050 3850 50  0001 C CNN
	1    5850 3850
	-1   0    0    1   
$EndComp
Wire Wire Line
	5850 3850 5850 3750
$Comp
L power:+5VA #PWR0197
U 1 1 5D80A89D
P 5850 3750
F 0 "#PWR0197" H 5850 3600 50  0001 C CNN
F 1 "+5VA" H 5850 3900 50  0000 C CNN
F 2 "" H 5850 3750 50  0001 C CNN
F 3 "" H 5850 3750 50  0001 C CNN
	1    5850 3750
	1    0    0    -1  
$EndComp
$Comp
L power:-5VA #PWR?
U 1 1 5D819737
P 6300 3850
AR Path="/5C5F098F/5D819737" Ref="#PWR?"  Part="1" 
AR Path="/5CBF1A8E/5D819737" Ref="#PWR?"  Part="1" 
AR Path="/5E105304/5D819737" Ref="#PWR0198"  Part="1" 
F 0 "#PWR0198" H 6300 3950 50  0001 C CNN
F 1 "-5VA" H 6300 4000 50  0000 C CNN
F 2 "" H 6300 3850 50  0001 C CNN
F 3 "" H 6300 3850 50  0001 C CNN
	1    6300 3850
	-1   0    0    1   
$EndComp
Wire Wire Line
	6300 3750 6300 3850
$Comp
L Connector:TestPoint TP30
U 1 1 5D819411
P 6300 3750
F 0 "TP30" H 6358 3868 50  0000 L CNN
F 1 "-5VA" H 6358 3777 50  0000 L CNN
F 2 "TestPoint:TestPoint_THTPad_D1.5mm_Drill0.7mm" H 6500 3750 50  0001 C CNN
F 3 "~" H 6500 3750 50  0001 C CNN
	1    6300 3750
	1    0    0    -1  
$EndComp
$Comp
L Device:CP1 C?
U 1 1 5D88A145
P 3700 5500
AR Path="/5CBF1A8E/5D88A145" Ref="C?"  Part="1" 
AR Path="/5E105304/5D88A145" Ref="C107"  Part="1" 
AR Path="/5C5F098F/5C5F0B20/5D88A145" Ref="C?"  Part="1" 
F 0 "C107" H 3815 5546 50  0000 L CNN
F 1 "22u" H 3815 5455 50  0000 L CNN
F 2 "Capacitor_Tantalum_SMD:CP_EIA-7343-31_Kemet-D" H 3700 5500 50  0001 C CNN
F 3 "http://datasheets.avx.com/TAJ.pdf" H 3700 5500 50  0001 C CNN
F 4 "TAJD226K020PNJ" H 3700 5500 50  0001 C CNN "PN"
F 5 "AVX" H -1050 1750 50  0001 C CNN "Manufacturer"
F 6 "10%" H 3700 5500 50  0001 C CNN "Tolerance"
F 7 "Ta" H 3700 5500 50  0001 C CNN "Dielectric"
F 8 "20V" H 3700 5500 50  0001 C CNN "Value2"
	1    3700 5500
	1    0    0    -1  
$EndComp
$Comp
L Device:CP1 C?
U 1 1 5D9EE72C
P 2700 5500
AR Path="/5CBF1A8E/5D9EE72C" Ref="C?"  Part="1" 
AR Path="/5E105304/5D9EE72C" Ref="C104"  Part="1" 
AR Path="/5C5F098F/5C5F0B20/5D9EE72C" Ref="C?"  Part="1" 
F 0 "C104" H 2815 5546 50  0000 L CNN
F 1 "22u" H 2815 5455 50  0000 L CNN
F 2 "Capacitor_Tantalum_SMD:CP_EIA-7343-31_Kemet-D" H 2700 5500 50  0001 C CNN
F 3 "http://datasheets.avx.com/TAJ.pdf" H 2700 5500 50  0001 C CNN
F 4 "TAJD226K020PNJ" H 2700 5500 50  0001 C CNN "PN"
F 5 "AVX" H -2050 1750 50  0001 C CNN "Manufacturer"
F 6 "10%" H 2700 5500 50  0001 C CNN "Tolerance"
F 7 "Ta" H 2700 5500 50  0001 C CNN "Dielectric"
F 8 "20V" H 2700 5500 50  0001 C CNN "Value2"
	1    2700 5500
	1    0    0    -1  
$EndComp
$Comp
L Device:CP1 C?
U 1 1 5DA0F240
P 2650 3800
AR Path="/5CBF1A8E/5DA0F240" Ref="C?"  Part="1" 
AR Path="/5E105304/5DA0F240" Ref="C96"  Part="1" 
F 0 "C96" H 2765 3846 50  0000 L CNN
F 1 "22u" H 2765 3755 50  0000 L CNN
F 2 "Capacitor_Tantalum_SMD:CP_EIA-7343-31_Kemet-D" H 2650 3800 50  0001 C CNN
F 3 "http://datasheets.avx.com/TAJ.pdf" H 2650 3800 50  0001 C CNN
F 4 "TAJD226K020PNJ" H 2650 3800 50  0001 C CNN "PN"
F 5 "AVX" H 500 50  50  0001 C CNN "Manufacturer"
F 6 "10%" H 2650 3800 50  0001 C CNN "Tolerance"
F 7 "Ta" H 2650 3800 50  0001 C CNN "Dielectric"
F 8 "20V" H 2650 3800 50  0001 C CNN "Value2"
	1    2650 3800
	1    0    0    -1  
$EndComp
$Comp
L Device:CP1 C?
U 1 1 5DA2D89E
P 3150 3800
AR Path="/5CBF1A8E/5DA2D89E" Ref="C?"  Part="1" 
AR Path="/5E105304/5DA2D89E" Ref="C116"  Part="1" 
F 0 "C116" H 3265 3846 50  0000 L CNN
F 1 "22u" H 3265 3755 50  0000 L CNN
F 2 "Capacitor_Tantalum_SMD:CP_EIA-7343-31_Kemet-D" H 3150 3800 50  0001 C CNN
F 3 "http://datasheets.avx.com/TAJ.pdf" H 3150 3800 50  0001 C CNN
F 4 "TAJD226K020PNJ" H 3150 3800 50  0001 C CNN "PN"
F 5 "AVX" H 1000 50  50  0001 C CNN "Manufacturer"
F 6 "10%" H 3150 3800 50  0001 C CNN "Tolerance"
F 7 "Ta" H 3150 3800 50  0001 C CNN "Dielectric"
F 8 "20V" H 3150 3800 50  0001 C CNN "Value2"
	1    3150 3800
	1    0    0    -1  
$EndComp
Wire Wire Line
	3150 3650 3150 3550
Wire Wire Line
	3150 3550 2650 3550
Connection ~ 2650 3550
Wire Wire Line
	2650 3550 2650 3650
Wire Wire Line
	2650 4050 3150 4050
Wire Wire Line
	3150 4050 3150 3950
Connection ~ 2650 4050
Wire Wire Line
	2650 4050 2650 4150
Wire Wire Line
	5000 7450 5000 7550
Wire Wire Line
	5000 6650 5000 6750
$Comp
L Device:CP1 C?
U 1 1 5DD4E9D6
P 5000 6900
AR Path="/5CBF1A8E/5DD4E9D6" Ref="C?"  Part="1" 
AR Path="/5E105304/5DD4E9D6" Ref="C118"  Part="1" 
AR Path="/5C5F098F/5C5F0B20/5DD4E9D6" Ref="C?"  Part="1" 
F 0 "C118" H 5115 6946 50  0000 L CNN
F 1 "10u" H 5115 6855 50  0000 L CNN
F 2 "Capacitor_Tantalum_SMD:CP_EIA-3216-18_Kemet-A" H 5000 6900 50  0001 C CNN
F 3 "http://datasheets.avx.com/TCJ.pdf" H 5000 6900 50  0001 C CNN
F 4 "TCJA106M025R0150" H 5000 6900 50  0001 C CNN "PN"
F 5 "AVX" H -2600 1900 50  0001 C CNN "Manufacturer"
F 6 "20%" H 5000 6900 50  0001 C CNN "Tolerance"
F 7 "Ta Polymer" H 5000 6900 50  0001 C CNN "Dielectric"
F 8 "25V" H 5000 6900 50  0001 C CNN "Value2"
	1    5000 6900
	1    0    0    -1  
$EndComp
$Comp
L Device:CP1 C?
U 1 1 5DD4E9DF
P 5000 7300
AR Path="/5CBF1A8E/5DD4E9DF" Ref="C?"  Part="1" 
AR Path="/5E105304/5DD4E9DF" Ref="C120"  Part="1" 
AR Path="/5C5F098F/5C5F0B20/5DD4E9DF" Ref="C?"  Part="1" 
F 0 "C120" H 5115 7346 50  0000 L CNN
F 1 "10u" H 5115 7255 50  0000 L CNN
F 2 "Capacitor_Tantalum_SMD:CP_EIA-3216-18_Kemet-A" H 5000 7300 50  0001 C CNN
F 3 "http://datasheets.avx.com/TCJ.pdf" H 5000 7300 50  0001 C CNN
F 4 "TCJA106M025R0150" H 5000 7300 50  0001 C CNN "PN"
F 5 "AVX" H -2600 1800 50  0001 C CNN "Manufacturer"
F 6 "20%" H 5000 7300 50  0001 C CNN "Tolerance"
F 7 "Ta Polymer" H 5000 7300 50  0001 C CNN "Dielectric"
F 8 "25V" H 5000 7300 50  0001 C CNN "Value2"
	1    5000 7300
	1    0    0    -1  
$EndComp
Wire Wire Line
	5000 7050 5000 7100
Connection ~ 5000 7100
Wire Wire Line
	5000 7100 5000 7150
Wire Wire Line
	5000 7100 5400 7100
Wire Wire Line
	5400 7100 5400 7200
$Comp
L Device:CP1 C?
U 1 1 5DD4E9ED
P 4600 7100
AR Path="/5CBF1A8E/5DD4E9ED" Ref="C?"  Part="1" 
AR Path="/5E105304/5DD4E9ED" Ref="C119"  Part="1" 
AR Path="/5C5F098F/5C5F0B20/5DD4E9ED" Ref="C?"  Part="1" 
F 0 "C119" H 4715 7146 50  0000 L CNN
F 1 "22u" H 4715 7055 50  0000 L CNN
F 2 "Capacitor_Tantalum_SMD:CP_EIA-7343-31_Kemet-D" H 4600 7100 50  0001 C CNN
F 3 "http://datasheets.avx.com/TAJ.pdf" H 4600 7100 50  0001 C CNN
F 4 "TAJD226K020PNJ" H 4600 7100 50  0001 C CNN "PN"
F 5 "AVX" H -150 3350 50  0001 C CNN "Manufacturer"
F 6 "10%" H 4600 7100 50  0001 C CNN "Tolerance"
F 7 "Ta" H 4600 7100 50  0001 C CNN "Dielectric"
F 8 "20V" H 4600 7100 50  0001 C CNN "Value2"
	1    4600 7100
	1    0    0    -1  
$EndComp
Wire Wire Line
	4600 6650 4600 6950
Wire Wire Line
	4600 7250 4600 7550
$Comp
L power:GNDA #PWR0202
U 1 1 5DD4E9F5
P 5400 7200
F 0 "#PWR0202" H 5400 6950 50  0001 C CNN
F 1 "GNDA" H 5400 7050 50  0000 C CNN
F 2 "" H 5400 7200 50  0001 C CNN
F 3 "" H 5400 7200 50  0001 C CNN
	1    5400 7200
	1    0    0    -1  
$EndComp
$Comp
L power:+5VA #PWR?
U 1 1 5DD4E9FB
P 4600 6550
AR Path="/5CBF1A8E/5DD4E9FB" Ref="#PWR?"  Part="1" 
AR Path="/5E105304/5DD4E9FB" Ref="#PWR0203"  Part="1" 
AR Path="/5C5F098F/5C5F0B20/5DD4E9FB" Ref="#PWR?"  Part="1" 
F 0 "#PWR0203" H 4600 6400 50  0001 C CNN
F 1 "+5VA" H 4600 6700 50  0000 C CNN
F 2 "" H 4600 6550 50  0001 C CNN
F 3 "" H 4600 6550 50  0001 C CNN
	1    4600 6550
	1    0    0    -1  
$EndComp
Wire Wire Line
	4600 6550 4600 6650
Connection ~ 4600 6650
$Comp
L power:-5VA #PWR?
U 1 1 5DD4EA03
P 4600 7650
AR Path="/5CBF1A8E/5DD4EA03" Ref="#PWR?"  Part="1" 
AR Path="/5E105304/5DD4EA03" Ref="#PWR0204"  Part="1" 
AR Path="/5C5F098F/5C5F0B20/5DD4EA03" Ref="#PWR?"  Part="1" 
F 0 "#PWR0204" H 4600 7750 50  0001 C CNN
F 1 "-5VA" H 4600 7800 50  0000 C CNN
F 2 "" H 4600 7650 50  0001 C CNN
F 3 "" H 4600 7650 50  0001 C CNN
	1    4600 7650
	-1   0    0    1   
$EndComp
Wire Wire Line
	4600 7650 4600 7550
Connection ~ 4600 7550
Wire Wire Line
	4600 6650 5000 6650
Wire Wire Line
	4600 7550 5000 7550
$Comp
L Device:CP1 C?
U 1 1 5CDA2AAE
P 3650 3800
AR Path="/5CBF1A8E/5CDA2AAE" Ref="C?"  Part="1" 
AR Path="/5E105304/5CDA2AAE" Ref="C122"  Part="1" 
F 0 "C122" H 3765 3846 50  0000 L CNN
F 1 "10u" H 3765 3755 50  0000 L CNN
F 2 "Capacitor_Tantalum_SMD:CP_EIA-3216-18_Kemet-A" H 3650 3800 50  0001 C CNN
F 3 "http://datasheets.avx.com/TCJ.pdf" H 3650 3800 50  0001 C CNN
F 4 "TCJA106M025R0150" H 3650 3800 50  0001 C CNN "PN"
F 5 "AVX" H -950 -1200 50  0001 C CNN "Manufacturer"
F 6 "20%" H 3650 3800 50  0001 C CNN "Tolerance"
F 7 "Ta Polymer" H 3650 3800 50  0001 C CNN "Dielectric"
F 8 "25V" H 3650 3800 50  0001 C CNN "Value2"
	1    3650 3800
	1    0    0    -1  
$EndComp
Wire Wire Line
	3150 3550 3650 3550
Wire Wire Line
	3650 3550 3650 3650
Wire Wire Line
	3650 4050 3150 4050
Wire Wire Line
	3650 3950 3650 4050
Connection ~ 3150 3550
Connection ~ 3150 4050
$Comp
L Device:CP1 C?
U 1 1 5CE43C3D
P 4700 5500
AR Path="/5CBF1A8E/5CE43C3D" Ref="C?"  Part="1" 
AR Path="/5E105304/5CE43C3D" Ref="C126"  Part="1" 
AR Path="/5C5F098F/5C5F0B20/5CE43C3D" Ref="C?"  Part="1" 
F 0 "C126" H 4815 5546 50  0000 L CNN
F 1 "10u" H 4815 5455 50  0000 L CNN
F 2 "Capacitor_Tantalum_SMD:CP_EIA-3216-18_Kemet-A" H 4700 5500 50  0001 C CNN
F 3 "http://datasheets.avx.com/TCJ.pdf" H 4700 5500 50  0001 C CNN
F 4 "TCJA106M025R0150" H 4700 5500 50  0001 C CNN "PN"
F 5 "AVX" H -2900 500 50  0001 C CNN "Manufacturer"
F 6 "20%" H 4700 5500 50  0001 C CNN "Tolerance"
F 7 "Ta Polymer" H 4700 5500 50  0001 C CNN "Dielectric"
F 8 "25V" H 4700 5500 50  0001 C CNN "Value2"
	1    4700 5500
	1    0    0    -1  
$EndComp
Wire Wire Line
	4200 5250 4700 5250
Wire Wire Line
	4700 5250 4700 5350
Wire Wire Line
	4700 5750 4200 5750
Wire Wire Line
	4700 5650 4700 5750
Connection ~ 4200 5250
Connection ~ 4200 5750
$Comp
L Device:CP1 C?
U 1 1 5CE5F036
P 5200 5500
AR Path="/5CBF1A8E/5CE5F036" Ref="C?"  Part="1" 
AR Path="/5E105304/5CE5F036" Ref="C127"  Part="1" 
AR Path="/5C5F098F/5C5F0B20/5CE5F036" Ref="C?"  Part="1" 
F 0 "C127" H 5315 5546 50  0000 L CNN
F 1 "10u" H 5315 5455 50  0000 L CNN
F 2 "Capacitor_Tantalum_SMD:CP_EIA-3216-18_Kemet-A" H 5200 5500 50  0001 C CNN
F 3 "http://datasheets.avx.com/TCJ.pdf" H 5200 5500 50  0001 C CNN
F 4 "TCJA106M025R0150" H 5200 5500 50  0001 C CNN "PN"
F 5 "AVX" H -2400 500 50  0001 C CNN "Manufacturer"
F 6 "20%" H 5200 5500 50  0001 C CNN "Tolerance"
F 7 "Ta Polymer" H 5200 5500 50  0001 C CNN "Dielectric"
F 8 "25V" H 5200 5500 50  0001 C CNN "Value2"
	1    5200 5500
	1    0    0    -1  
$EndComp
Wire Wire Line
	4700 5250 5200 5250
Wire Wire Line
	5200 5250 5200 5350
Wire Wire Line
	5200 5750 4700 5750
Wire Wire Line
	5200 5650 5200 5750
Connection ~ 4700 5250
Connection ~ 4700 5750
$Comp
L Device:CP1 C?
U 1 1 5CEBD946
P 5700 5500
AR Path="/5CBF1A8E/5CEBD946" Ref="C?"  Part="1" 
AR Path="/5E105304/5CEBD946" Ref="C128"  Part="1" 
AR Path="/5C5F098F/5C5F0B20/5CEBD946" Ref="C?"  Part="1" 
F 0 "C128" H 5815 5546 50  0000 L CNN
F 1 "10u" H 5815 5455 50  0000 L CNN
F 2 "Capacitor_Tantalum_SMD:CP_EIA-3216-18_Kemet-A" H 5700 5500 50  0001 C CNN
F 3 "http://datasheets.avx.com/TCJ.pdf" H 5700 5500 50  0001 C CNN
F 4 "TCJA106M025R0150" H 5700 5500 50  0001 C CNN "PN"
F 5 "AVX" H -1900 500 50  0001 C CNN "Manufacturer"
F 6 "20%" H 5700 5500 50  0001 C CNN "Tolerance"
F 7 "Ta Polymer" H 5700 5500 50  0001 C CNN "Dielectric"
F 8 "25V" H 5700 5500 50  0001 C CNN "Value2"
	1    5700 5500
	1    0    0    -1  
$EndComp
Wire Wire Line
	5200 5250 5700 5250
Wire Wire Line
	5700 5250 5700 5350
Wire Wire Line
	5700 5750 5200 5750
Wire Wire Line
	5700 5650 5700 5750
Connection ~ 5200 5250
Connection ~ 5200 5750
$Comp
L Device:CP1 C?
U 1 1 5CEEEBCD
P 6200 5500
AR Path="/5CBF1A8E/5CEEEBCD" Ref="C?"  Part="1" 
AR Path="/5E105304/5CEEEBCD" Ref="C129"  Part="1" 
AR Path="/5C5F098F/5C5F0B20/5CEEEBCD" Ref="C?"  Part="1" 
F 0 "C129" H 6315 5546 50  0000 L CNN
F 1 "10u" H 6315 5455 50  0000 L CNN
F 2 "Capacitor_Tantalum_SMD:CP_EIA-3216-18_Kemet-A" H 6200 5500 50  0001 C CNN
F 3 "http://datasheets.avx.com/TCJ.pdf" H 6200 5500 50  0001 C CNN
F 4 "TCJA106M025R0150" H 6200 5500 50  0001 C CNN "PN"
F 5 "AVX" H -1400 500 50  0001 C CNN "Manufacturer"
F 6 "20%" H 6200 5500 50  0001 C CNN "Tolerance"
F 7 "Ta Polymer" H 6200 5500 50  0001 C CNN "Dielectric"
F 8 "25V" H 6200 5500 50  0001 C CNN "Value2"
	1    6200 5500
	1    0    0    -1  
$EndComp
Wire Wire Line
	5700 5250 6200 5250
Wire Wire Line
	6200 5250 6200 5350
Wire Wire Line
	6200 5750 5700 5750
Wire Wire Line
	6200 5650 6200 5750
Connection ~ 5700 5250
Connection ~ 5700 5750
$Comp
L Device:R_Small R?
U 1 1 5DA65527
P 2750 1750
AR Path="/5CBF1A8E/5DA65527" Ref="R?"  Part="1" 
AR Path="/5E105304/5DA65527" Ref="R46"  Part="1" 
F 0 "R46" H 2809 1796 50  0000 L CNN
F 1 "7.5k" H 2809 1705 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 2750 1750 50  0001 C CNN
F 3 "~" H 2750 1750 50  0001 C CNN
F 4 "Yageo" H -500 -300 50  0001 C CNN "Manufacturer"
F 5 "RC0603FR-077K5" H -500 -300 50  0001 C CNN "PN"
F 6 "1%" H -500 -300 50  0001 C CNN "Tolerance"
	1    2750 1750
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R?
U 1 1 5DA64AF4
P 2750 2050
AR Path="/5CBF1A8E/5DA64AF4" Ref="R?"  Part="1" 
AR Path="/5E105304/5DA64AF4" Ref="R50"  Part="1" 
F 0 "R50" H 2809 2096 50  0000 L CNN
F 1 "1k" H 2809 2005 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 2750 2050 50  0001 C CNN
F 3 "~" H 2750 2050 50  0001 C CNN
F 4 "Yageo" H -500 300 50  0001 C CNN "Manufacturer"
F 5 "RC0603FR-071K" H -500 300 50  0001 C CNN "PN"
F 6 "1%" H -500 300 50  0001 C CNN "Tolerance"
	1    2750 2050
	1    0    0    -1  
$EndComp
Text Notes 2800 1550 0    50   ~ 0
(1)
Text Notes 6300 7350 0    50   ~ 0
(1) +5VA = GNDD + 10.3075V
$EndSCHEMATC
