EESchema Schematic File Version 4
LIBS:Capstone-cache
EELAYER 29 0
EELAYER END
$Descr USLetter 11000 8500
encoding utf-8
Sheet 1 8
Title "Capstone"
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Memory_EEPROM:CAT24C256 U1
U 1 1 5C4CEA37
P 7900 4450
F 0 "U1" H 7600 4700 50  0000 L CNN
F 1 "CAT24C256" H 8150 4200 50  0000 C CNN
F 2 "Package_SO:TSSOP-8_4.4x3mm_P0.65mm" H 7900 4450 50  0001 C CNN
F 3 "https://www.onsemi.cn/PowerSolutions/document/CAT24C256-D.PDF" H 7900 4450 50  0001 C CNN
F 4 "ON Semiconductor" H 5650 -1500 50  0001 C CNN "Manufacturer"
F 5 "CAT24C256YI" H 5650 -1500 50  0001 C CNN "PN"
	1    7900 4450
	1    0    0    -1  
$EndComp
Wire Wire Line
	8400 4550 8300 4550
Text GLabel 4300 3850 0    50   BiDi ~ 0
SCL
Wire Wire Line
	4300 3850 4400 3850
Text GLabel 4300 3950 0    50   BiDi ~ 0
SDA
Wire Wire Line
	4300 3950 4400 3950
$Comp
L power:GNDD #PWR03
U 1 1 5C781C95
P 3450 7250
F 0 "#PWR03" H 3450 7000 50  0001 C CNN
F 1 "GNDD" H 3450 7100 50  0000 C CNN
F 2 "" H 3450 7250 50  0001 C CNN
F 3 "" H 3450 7250 50  0001 C CNN
	1    3450 7250
	1    0    0    -1  
$EndComp
$Comp
L power:GNDD #PWR09
U 1 1 5C782533
P 7900 4750
F 0 "#PWR09" H 7900 4500 50  0001 C CNN
F 1 "GNDD" H 7900 4600 50  0000 C CNN
F 2 "" H 7900 4750 50  0001 C CNN
F 3 "" H 7900 4750 50  0001 C CNN
	1    7900 4750
	1    0    0    -1  
$EndComp
Wire Wire Line
	8300 4450 8500 4450
Text GLabel 8500 4450 2    50   BiDi ~ 0
SCL
Wire Wire Line
	8300 4350 8500 4350
Text GLabel 8500 4350 2    50   BiDi ~ 0
SDA
Text Notes 8750 4450 0    50   ~ 0
(1)
Wire Wire Line
	7500 4350 7450 4350
Wire Wire Line
	7500 4450 7450 4450
Wire Wire Line
	7500 4550 7450 4550
NoConn ~ 7450 4350
NoConn ~ 7450 4450
NoConn ~ 7450 4550
Text Notes 7350 4250 0    50   ~ 0
(2)
Text Notes 6300 7250 0    50   ~ 0
(2) Address pins have weak internal pull-downs
Text Notes 8300 4050 0    50   ~ 0
(3)
Text Notes 6300 7150 0    50   ~ 0
(3) Short jumper to enable write protection; WP pin has a weak internal pull-down
$Comp
L Connector:Conn_01x04_Male J2
U 1 1 5CE2E36A
P 9400 4350
F 0 "J2" H 9300 4300 50  0000 C CNN
F 1 "Conn_01x04_Male" H 9508 4540 50  0001 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x04_P2.54mm_Vertical" H 9400 4350 50  0001 C CNN
F 3 "~" H 9400 4350 50  0001 C CNN
	1    9400 4350
	1    0    0    -1  
$EndComp
Wire Wire Line
	9600 4250 9700 4250
Wire Wire Line
	9600 4450 9700 4450
Wire Wire Line
	9600 4550 9800 4550
Wire Wire Line
	9600 4350 9800 4350
Wire Wire Line
	9700 4250 9700 4150
Wire Wire Line
	9700 4450 9700 4650
$Comp
L power:GNDD #PWR014
U 1 1 5CE39C06
P 9700 4650
F 0 "#PWR014" H 9700 4400 50  0001 C CNN
F 1 "GNDD" H 9700 4500 50  0000 C CNN
F 2 "" H 9700 4650 50  0001 C CNN
F 3 "" H 9700 4650 50  0001 C CNN
	1    9700 4650
	1    0    0    -1  
$EndComp
Text GLabel 9800 4350 2    50   BiDi ~ 0
SCL
Text GLabel 9800 4550 2    50   BiDi ~ 0
SDA
$Comp
L power:+3V3 #PWR013
U 1 1 5CE3CA5F
P 9700 4150
F 0 "#PWR013" H 9700 4000 50  0001 C CNN
F 1 "+3V3" H 9700 4300 50  0000 C CNN
F 2 "" H 9700 4150 50  0001 C CNN
F 3 "" H 9700 4150 50  0001 C CNN
	1    9700 4150
	1    0    0    -1  
$EndComp
Text Notes 7400 3800 0    50   ~ 0
Card info EEPROM (32KiB)
Text Notes 9200 3900 0    50   ~ 0
I2C Test/Program Port
$Comp
L power:+3V3 #PWR05
U 1 1 5D71D8B4
P 7900 4050
F 0 "#PWR05" H 7900 3900 50  0001 C CNN
F 1 "+3V3" H 7900 4200 50  0000 C CNN
F 2 "" H 7900 4050 50  0001 C CNN
F 3 "" H 7900 4050 50  0001 C CNN
	1    7900 4050
	1    0    0    -1  
$EndComp
Wire Wire Line
	8400 4100 8350 4100
Wire Wire Line
	8400 4100 8400 4550
Wire Wire Line
	7900 4150 7900 4100
Wire Wire Line
	7900 4100 7950 4100
Wire Wire Line
	7900 4100 7900 4050
Connection ~ 7900 4100
$Comp
L Jumper:Jumper_2_Open JP1
U 1 1 5C72F313
P 8150 4100
F 0 "JP1" H 8150 4250 50  0000 C CNN
F 1 "Jumper_2_Open" V 8195 4198 50  0001 L CNN
F 2 "Jumper:SolderJumper-2_P1.3mm_Open_Pad1.0x1.5mm" H 8150 4100 50  0001 C CNN
F 3 "~" H 8150 4100 50  0001 C CNN
	1    8150 4100
	1    0    0    -1  
$EndComp
$Sheet
S 7950 1450 1300 1800
U 5C5F098F
F0 "Analog Signal Chain" 50
F1 "AnalogChain.sch" 50
F2 "MOSI-" I L 7950 2550 50 
F3 "MOSI+" I L 7950 2450 50 
F4 "SCLK_W+" I L 7950 2150 50 
F5 "SCLK_W-" I L 7950 2250 50 
F6 "MISO+" O L 7950 3050 50 
F7 "MISO-" O L 7950 3150 50 
F8 "SCLK_R+" O L 7950 2750 50 
F9 "SCLK_R-" O L 7950 2850 50 
F10 "~CS_SLOPE~-" I L 7950 1650 50 
F11 "~CS_SLOPE~+" I L 7950 1550 50 
F12 "~CS_REF~+" I L 7950 1850 50 
F13 "~CS_REF~-" I L 7950 1950 50 
F14 "~INT" O R 9250 1950 50 
F15 "COMP+" O R 9250 1550 50 
F16 "COMP-" O R 9250 1650 50 
F17 "IntSum1.REF[1]" I R 9250 2450 50 
F18 "IntSum1.REF[0]" I R 9250 2550 50 
F19 "IntSum1.~RESET" I R 9250 2150 50 
F20 "IntSum2.REF[1]" I R 9250 2750 50 
F21 "IntSum2.REF[0]" I R 9250 2850 50 
F22 "IntSum2.~RESET" I R 9250 2250 50 
$EndSheet
NoConn ~ 4400 1150
NoConn ~ 4400 1250
NoConn ~ 4400 1450
NoConn ~ 4400 1550
NoConn ~ 4400 1750
NoConn ~ 4400 1850
NoConn ~ 4400 2050
NoConn ~ 4400 2150
NoConn ~ 4400 2350
NoConn ~ 4400 2450
NoConn ~ 4400 2650
NoConn ~ 4400 2750
NoConn ~ 4400 2950
NoConn ~ 4400 3050
NoConn ~ 4400 3250
NoConn ~ 4400 3350
NoConn ~ 4400 3650
NoConn ~ 6200 3350
NoConn ~ 6200 3250
NoConn ~ 6200 3050
NoConn ~ 6200 2950
NoConn ~ 6200 2750
NoConn ~ 6200 2650
NoConn ~ 6200 2450
NoConn ~ 6200 2350
NoConn ~ 6200 2150
NoConn ~ 6200 2050
NoConn ~ 6200 1850
NoConn ~ 6200 1750
NoConn ~ 6200 1550
NoConn ~ 6200 1450
NoConn ~ 6200 1250
NoConn ~ 6200 1150
Wire Wire Line
	7950 1550 7450 1550
Text Label 7450 1550 0    50   ~ 0
~CS_SLOPE~+
Wire Wire Line
	7450 1650 7950 1650
Text Label 7450 1650 0    50   ~ 0
~CS_SLOPE~-
Wire Wire Line
	7450 1850 7950 1850
Text Label 7450 1850 0    50   ~ 0
~CS_REF~+
Text Label 7450 1950 0    50   ~ 0
~CS_REF~-
Text Label 7450 2150 0    50   ~ 0
SCLK_W+
Text Label 7450 2250 0    50   ~ 0
SCLK_W-
Text Label 7450 2450 0    50   ~ 0
MOSI+
Text Label 7450 2550 0    50   ~ 0
MOSI-
Text Label 7450 2750 0    50   ~ 0
SCLK_R+
Text Label 7450 2850 0    50   ~ 0
SCLK_R-
Text Label 7450 3050 0    50   ~ 0
MISO+
Text Label 7450 3150 0    50   ~ 0
MISO-
Wire Wire Line
	9250 2150 9950 2150
Wire Wire Line
	7450 1950 7950 1950
Wire Wire Line
	7450 2150 7950 2150
Wire Wire Line
	7450 2250 7950 2250
Wire Wire Line
	7450 2450 7950 2450
Wire Wire Line
	7450 2550 7950 2550
Wire Wire Line
	7450 2750 7950 2750
Wire Wire Line
	7450 2850 7950 2850
Wire Wire Line
	7450 3050 7950 3050
Wire Wire Line
	7450 3150 7950 3150
Text Label 9950 2150 2    50   ~ 0
IntSum1.~RESET
Wire Wire Line
	9250 2250 9950 2250
Wire Wire Line
	9250 2450 9950 2450
Wire Wire Line
	9250 2550 9950 2550
Wire Wire Line
	9250 2750 9950 2750
Wire Wire Line
	9250 2850 9950 2850
Text Label 9950 2250 2    50   ~ 0
IntSum2.~RESET
Text Label 9950 2450 2    50   ~ 0
IntSum1.REF[1]
Text Label 9950 2550 2    50   ~ 0
IntSum1.REF[0]
Text Label 9950 2750 2    50   ~ 0
IntSum2.REF[1]
Text Label 9950 2850 2    50   ~ 0
IntSum2.REF[0]
Wire Wire Line
	9250 1950 9950 1950
Text Label 9950 1950 2    50   ~ 0
~INT
Text Notes 6300 7350 0    50   ~ 0
(1) I2C interface should be routed to I2C1 on the HPS in the FPGA
$Sheet
S 7950 950  1300 250 
U 5E105304
F0 "Power Supplies" 50
F1 "Power.sch" 50
$EndSheet
NoConn ~ 3350 1150
NoConn ~ 3350 1250
NoConn ~ 1550 1250
NoConn ~ 3350 2050
NoConn ~ 3350 2150
NoConn ~ 1550 2350
NoConn ~ 1550 2450
NoConn ~ 1550 2650
NoConn ~ 1550 2750
NoConn ~ 3350 2350
NoConn ~ 3350 2450
NoConn ~ 3350 2650
NoConn ~ 3350 2750
NoConn ~ 1550 5650
NoConn ~ 1550 5750
NoConn ~ 1550 5950
NoConn ~ 1550 6050
NoConn ~ 1550 6250
NoConn ~ 1550 6350
NoConn ~ 1550 6850
NoConn ~ 1550 6950
NoConn ~ 3350 6950
NoConn ~ 3350 6850
NoConn ~ 3350 6650
NoConn ~ 3350 6550
NoConn ~ 3350 6350
NoConn ~ 3350 6250
NoConn ~ 3350 6050
NoConn ~ 3350 5950
NoConn ~ 3350 5450
NoConn ~ 3350 5350
NoConn ~ 3350 5150
$Comp
L Diode:BAT54S D2
U 1 1 5F1EE1C5
P 8650 6000
F 0 "D2" V 8696 6088 50  0000 L CNN
F 1 "BAT54SW" V 8605 6088 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 8725 6125 50  0001 L CNN
F 3 "https://www.onsemi.com/pub/Collateral/BAT54SWT1-D.PDF" H 8530 6000 50  0001 C CNN
F 4 "ROHM" V 8650 6000 50  0001 C CNN "Manufacturer"
F 5 "BAT54SHMFH" V 8650 6000 50  0001 C CNN "PN"
	1    8650 6000
	0    1    -1   0   
$EndComp
$Comp
L power:GNDD #PWR011
U 1 1 5F1F134D
P 8650 6300
F 0 "#PWR011" H 8650 6050 50  0001 C CNN
F 1 "GNDD" H 8650 6150 50  0000 C CNN
F 2 "" H 8650 6300 50  0001 C CNN
F 3 "" H 8650 6300 50  0001 C CNN
	1    8650 6300
	1    0    0    -1  
$EndComp
Text GLabel 8150 6000 0    50   BiDi ~ 0
SDA
Text GLabel 6900 6000 0    50   BiDi ~ 0
SCL
$Comp
L Diode:BAT54S D1
U 1 1 5F1F88D8
P 7400 6000
F 0 "D1" V 7446 6088 50  0000 L CNN
F 1 "BAT54SW" V 7355 6088 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 7475 6125 50  0001 L CNN
F 3 "https://www.onsemi.com/pub/Collateral/BAT54SWT1-D.PDF" H 7280 6000 50  0001 C CNN
F 4 "ROHM" V 7400 6000 50  0001 C CNN "Manufacturer"
F 5 "BAT54SHMFH" V 7400 6000 50  0001 C CNN "PN"
	1    7400 6000
	0    1    -1   0   
$EndComp
$Comp
L power:GNDD #PWR010
U 1 1 5F1F88DE
P 7400 6300
F 0 "#PWR010" H 7400 6050 50  0001 C CNN
F 1 "GNDD" H 7400 6150 50  0000 C CNN
F 2 "" H 7400 6300 50  0001 C CNN
F 3 "" H 7400 6300 50  0001 C CNN
	1    7400 6300
	1    0    0    -1  
$EndComp
$Comp
L power:+3V3 #PWR06
U 1 1 5F1FC652
P 7400 5600
F 0 "#PWR06" H 7400 5450 50  0001 C CNN
F 1 "+3V3" H 7400 5750 50  0000 C CNN
F 2 "" H 7400 5600 50  0001 C CNN
F 3 "" H 7400 5600 50  0001 C CNN
	1    7400 5600
	1    0    0    -1  
$EndComp
Text Notes 7600 6200 2    50   ~ 0
(4)
Text Notes 6300 7050 0    50   ~ 0
(4) Place all protection diodes as close as possible to HSMC pins. The FPGA will be configured for 2.5V\n    I/O. It can accept 3.3V input signals, but only with the internal clamp diodes disabled.
$Comp
L Device:R_Small R?
U 1 1 5F2B87EF
P 7000 5800
AR Path="/5CBF1A8E/5F2B87EF" Ref="R?"  Part="1" 
AR Path="/5E105304/5F2B87EF" Ref="R?"  Part="1" 
AR Path="/5F2B87EF" Ref="R1"  Part="1" 
F 0 "R1" H 7059 5846 50  0000 L CNN
F 1 "1k" H 7059 5755 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 7000 5800 50  0001 C CNN
F 3 "~" H 7000 5800 50  0001 C CNN
F 4 "Yageo" H -400 2200 50  0001 C CNN "Manufacturer"
F 5 "RC0603JR-071K" H -400 2200 50  0001 C CNN "PN"
F 6 "5%" H -400 2200 50  0001 C CNN "Tolerance"
	1    7000 5800
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R?
U 1 1 5F2B87F8
P 8250 5800
AR Path="/5CBF1A8E/5F2B87F8" Ref="R?"  Part="1" 
AR Path="/5E105304/5F2B87F8" Ref="R?"  Part="1" 
AR Path="/5F2B87F8" Ref="R2"  Part="1" 
F 0 "R2" H 8309 5846 50  0000 L CNN
F 1 "1k" H 8309 5755 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 8250 5800 50  0001 C CNN
F 3 "~" H 8250 5800 50  0001 C CNN
F 4 "Yageo" H 550 2200 50  0001 C CNN "Manufacturer"
F 5 "RC0603JR-071K" H 550 2200 50  0001 C CNN "PN"
F 6 "5%" H 550 2200 50  0001 C CNN "Tolerance"
	1    8250 5800
	1    0    0    -1  
$EndComp
Wire Wire Line
	8150 6000 8250 6000
Wire Wire Line
	8250 6000 8250 5900
Connection ~ 8250 6000
Wire Wire Line
	8250 6000 8450 6000
$Comp
L power:+3V3 #PWR07
U 1 1 5F1F12BA
P 8650 5600
F 0 "#PWR07" H 8650 5450 50  0001 C CNN
F 1 "+3V3" H 8650 5750 50  0000 C CNN
F 2 "" H 8650 5600 50  0001 C CNN
F 3 "" H 8650 5600 50  0001 C CNN
	1    8650 5600
	1    0    0    -1  
$EndComp
Wire Wire Line
	8250 5700 8250 5650
Wire Wire Line
	8250 5650 8650 5650
Wire Wire Line
	8650 5600 8650 5650
Connection ~ 8650 5650
Wire Wire Line
	8650 5650 8650 5700
Wire Wire Line
	7400 5600 7400 5650
Wire Wire Line
	7400 5650 7000 5650
Wire Wire Line
	7000 5650 7000 5700
Connection ~ 7400 5650
Wire Wire Line
	7400 5650 7400 5700
Wire Wire Line
	7000 5900 7000 6000
Wire Wire Line
	7000 6000 7200 6000
Wire Wire Line
	6900 6000 7000 6000
Connection ~ 7000 6000
NoConn ~ 1550 2050
NoConn ~ 1550 2150
NoConn ~ 1550 2950
NoConn ~ 1550 3050
NoConn ~ 3350 1450
NoConn ~ 3350 1550
NoConn ~ 3350 1750
NoConn ~ 3350 1850
Text Label 850  1150 0    50   ~ 0
~INT
Wire Wire Line
	1550 1150 850  1150
Text Label 850  4850 0    50   ~ 0
IntSum2.~RESET
Wire Wire Line
	1550 4750 850  4750
Text Label 850  4750 0    50   ~ 0
IntSum1.~RESET
Wire Wire Line
	1550 4850 850  4850
Text Label 1050 3550 0    50   ~ 0
MISO+
Wire Wire Line
	1050 3550 1550 3550
Wire Wire Line
	1050 3650 1550 3650
Wire Wire Line
	1050 3950 1550 3950
Wire Wire Line
	1050 3850 1550 3850
Text Label 1050 3650 0    50   ~ 0
MISO-
Text Label 1050 3850 0    50   ~ 0
SCLK_R-
Text Label 1050 3950 0    50   ~ 0
SCLK_R+
Wire Wire Line
	3850 3950 3350 3950
Wire Wire Line
	3850 3850 3350 3850
Text Label 3850 3950 2    50   ~ 0
SCLK_W-
Text Label 3850 3850 2    50   ~ 0
SCLK_W+
$Comp
L power:GNDD #PWR04
U 1 1 5C781BE2
P 5300 4450
F 0 "#PWR04" H 5300 4200 50  0001 C CNN
F 1 "GNDD" H 5300 4300 50  0000 C CNN
F 2 "" H 5300 4450 50  0001 C CNN
F 3 "" H 5300 4450 50  0001 C CNN
	1    5300 4450
	1    0    0    -1  
$EndComp
Wire Wire Line
	3450 7250 3450 7150
Wire Wire Line
	3450 7150 3350 7150
NoConn ~ 3350 5050
NoConn ~ 6200 3650
NoConn ~ 6200 4150
NoConn ~ 6200 3850
Wire Wire Line
	9250 1550 9950 1550
Text Label 9950 1550 2    50   ~ 0
COMP+
NoConn ~ 1550 3250
NoConn ~ 1550 3350
Wire Wire Line
	3850 4450 3350 4450
Text Label 3850 4450 2    50   ~ 0
~CS_REF~+
Text Label 3850 4550 2    50   ~ 0
~CS_REF~-
Wire Wire Line
	3850 4550 3350 4550
NoConn ~ 3350 5750
Wire Wire Line
	3850 4250 3350 4250
Wire Wire Line
	3850 4150 3350 4150
Text Label 3850 4250 2    50   ~ 0
MOSI-
Text Label 3850 4150 2    50   ~ 0
MOSI+
NoConn ~ 3350 5650
Text Label 3850 4850 2    50   ~ 0
~CS_SLOPE~-
Wire Wire Line
	3850 4850 3350 4850
Text Label 3850 4750 2    50   ~ 0
~CS_SLOPE~+
Wire Wire Line
	3350 4750 3850 4750
Wire Wire Line
	9250 1650 9950 1650
Text Label 9950 1650 2    50   ~ 0
COMP-
Text Label 1050 6550 0    50   ~ 0
COMP+
Wire Wire Line
	1050 6550 1550 6550
Wire Wire Line
	1050 6650 1550 6650
Text Label 1050 6650 0    50   ~ 0
COMP-
NoConn ~ 1550 5450
Text Label 850  1450 0    50   ~ 0
IntSum1.REF[1]
Wire Wire Line
	1550 1450 850  1450
Text Label 850  1550 0    50   ~ 0
IntSum1.REF[0]
Wire Wire Line
	1550 1550 850  1550
Wire Wire Line
	1550 4550 850  4550
Wire Wire Line
	1550 4450 850  4450
Text Label 850  4450 0    50   ~ 0
IntSum2.REF[1]
Text Label 850  4550 0    50   ~ 0
IntSum2.REF[0]
NoConn ~ 1550 5350
NoConn ~ 1550 4150
NoConn ~ 1550 4250
NoConn ~ 1550 5050
NoConn ~ 1550 5150
NoConn ~ 3350 3250
NoConn ~ 3350 3350
NoConn ~ 3350 3550
NoConn ~ 3350 3650
NoConn ~ 3350 2950
NoConn ~ 3350 3050
NoConn ~ 1550 1750
NoConn ~ 1550 1850
NoConn ~ 6200 4050
NoConn ~ 6200 3950
$Comp
L Device:C C?
U 1 1 5DD9DB08
P 7000 4450
AR Path="/5C5F098F/5C5F0B20/5DD9DB08" Ref="C?"  Part="1" 
AR Path="/5C5F098F/5C5F0B22/5DD9DB08" Ref="C?"  Part="1" 
AR Path="/5C5F098F/5CB3F455/5DD9DB08" Ref="C?"  Part="1" 
AR Path="/5C5F098F/5DD9DB08" Ref="C?"  Part="1" 
AR Path="/5DD9DB08" Ref="C121"  Part="1" 
F 0 "C121" H 7115 4496 50  0000 L CNN
F 1 "100n" H 7115 4405 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 7038 4300 50  0001 C CNN
F 3 "~" H 7000 4450 50  0001 C CNN
F 4 "Murata" H 7000 4450 50  0001 C CNN "Manufacturer"
F 5 "GRM188R71C104KA01D" H 7000 4450 50  0001 C CNN "PN"
F 6 "10%" H 7000 4450 50  0001 C CNN "Tolerance"
F 7 "X7R" H 7000 4450 50  0001 C CNN "Dielectric"
F 8 "16V" H 7000 4450 50  0001 C CNN "Value2"
	1    7000 4450
	1    0    0    -1  
$EndComp
$Comp
L power:+3V3 #PWR0205
U 1 1 5DDA1A59
P 7000 4200
F 0 "#PWR0205" H 7000 4050 50  0001 C CNN
F 1 "+3V3" H 7000 4350 50  0000 C CNN
F 2 "" H 7000 4200 50  0001 C CNN
F 3 "" H 7000 4200 50  0001 C CNN
	1    7000 4200
	1    0    0    -1  
$EndComp
$Comp
L power:GNDD #PWR0206
U 1 1 5DDA207F
P 7000 4700
F 0 "#PWR0206" H 7000 4450 50  0001 C CNN
F 1 "GNDD" H 7000 4550 50  0000 C CNN
F 2 "" H 7000 4700 50  0001 C CNN
F 3 "" H 7000 4700 50  0001 C CNN
	1    7000 4700
	1    0    0    -1  
$EndComp
Wire Wire Line
	7000 4700 7000 4600
Wire Wire Line
	7000 4300 7000 4200
$Comp
L Connector:HSMC_Card_QTH-090-01-x-D-A J1
U 2 1 5CE71483
P 2450 4150
F 0 "J1" H 2450 7450 50  0000 C CNN
F 1 "HSMC_Card_QTH-090-01-x-D-A" H 2450 7350 50  0000 C CNN
F 2 "Connector_Samtec:Samtec_QTH-090-01-x-D-A_P0.5mm_Vertical" H 2450 750 50  0001 C CNN
F 3 "https://www.intel.com/content/dam/www/programmable/us/en/pdfs/literature/ds/hsmc_spec.pdf" H 2500 7300 50  0001 C CNN
	2    2450 4150
	1    0    0    -1  
$EndComp
$Comp
L Connector:HSMC_Card_QTH-090-01-x-D-A J1
U 1 1 5CE57FAC
P 5300 2650
F 0 "J1" H 4550 4350 50  0000 L CNN
F 1 "HSMC_Card_QTH-090-01-x-D-A" H 6250 4550 50  0001 C CNN
F 2 "Connector_Samtec:Samtec_QTH-090-01-x-D-A_P0.5mm_Vertical" H 5300 -750 50  0001 C CNN
F 3 "https://www.intel.com/content/dam/www/programmable/us/en/pdfs/literature/ds/hsmc_spec.pdf" H 5350 5800 50  0001 C CNN
	1    5300 2650
	1    0    0    -1  
$EndComp
$Comp
L Connector:HSMC_Card_QTH-090-01-x-D-A J1
U 3 1 5CF4EDE4
P 5300 5500
F 0 "J1" H 5300 6050 50  0000 C CNN
F 1 "HSMC_Card_QTH-090-01-x-D-A" H 6250 7400 50  0001 C CNN
F 2 "Connector_Samtec:Samtec_QTH-090-01-x-D-A_P0.5mm_Vertical" H 5300 2100 50  0001 C CNN
F 3 "https://www.intel.com/content/dam/www/programmable/us/en/pdfs/literature/ds/hsmc_spec.pdf" H 5350 8650 50  0001 C CNN
	3    5300 5500
	1    0    0    -1  
$EndComp
Wire Wire Line
	5000 5100 5000 5200
Connection ~ 5000 5200
Wire Wire Line
	5000 5200 5000 5300
Connection ~ 5000 5300
Wire Wire Line
	5000 5300 5000 5400
Connection ~ 5000 5400
Wire Wire Line
	5000 5400 5000 5500
Connection ~ 5000 5500
Wire Wire Line
	5000 5500 5000 5600
Connection ~ 5000 5600
Wire Wire Line
	5000 5600 5000 5700
Connection ~ 5000 5700
Wire Wire Line
	5000 5700 5000 5800
Connection ~ 5000 5800
Wire Wire Line
	5000 5800 5000 5900
Connection ~ 5000 5900
Wire Wire Line
	5000 5900 5000 6000
Connection ~ 5000 6000
Wire Wire Line
	5000 6000 5000 6200
$Comp
L power:GNDD #PWR0207
U 1 1 5CF80889
P 5300 6300
F 0 "#PWR0207" H 5300 6050 50  0001 C CNN
F 1 "GNDD" H 5300 6150 50  0000 C CNN
F 2 "" H 5300 6300 50  0001 C CNN
F 3 "" H 5300 6300 50  0001 C CNN
	1    5300 6300
	1    0    0    -1  
$EndComp
Wire Wire Line
	5400 800  5400 850 
Wire Wire Line
	5450 800  5400 800 
$Comp
L power:+12V #PWR02
U 1 1 5C4775E4
P 5450 750
F 0 "#PWR02" H 5450 600 50  0001 C CNN
F 1 "+12V" H 5450 900 50  0000 C CNN
F 2 "" H 5450 750 50  0001 C CNN
F 3 "" H 5450 750 50  0001 C CNN
	1    5450 750 
	1    0    0    -1  
$EndComp
Wire Wire Line
	5450 750  5450 800 
$Comp
L power:+3V3 #PWR01
U 1 1 5D71E544
P 5150 750
F 0 "#PWR01" H 5150 600 50  0001 C CNN
F 1 "+3V3" H 5150 900 50  0000 C CNN
F 2 "" H 5150 750 50  0001 C CNN
F 3 "" H 5150 750 50  0001 C CNN
	1    5150 750 
	1    0    0    -1  
$EndComp
Wire Wire Line
	5150 750  5150 800 
Wire Wire Line
	5150 800  5200 800 
Wire Wire Line
	5200 800  5200 850 
Wire Wire Line
	5000 6200 5300 6200
Wire Wire Line
	5600 6200 5600 6000
Wire Wire Line
	5300 6200 5300 6300
Connection ~ 5600 5200
Wire Wire Line
	5600 5200 5600 5100
Connection ~ 5600 5300
Wire Wire Line
	5600 5300 5600 5200
Connection ~ 5600 5400
Wire Wire Line
	5600 5400 5600 5300
Connection ~ 5600 5500
Wire Wire Line
	5600 5500 5600 5400
Connection ~ 5600 5600
Wire Wire Line
	5600 5600 5600 5500
Connection ~ 5600 5700
Wire Wire Line
	5600 5700 5600 5600
Connection ~ 5600 5800
Wire Wire Line
	5600 5800 5600 5700
Connection ~ 5600 5900
Wire Wire Line
	5600 5900 5600 5800
Connection ~ 5600 6000
Wire Wire Line
	5600 6000 5600 5900
Connection ~ 5300 6200
Wire Wire Line
	5300 6200 5600 6200
Wire Wire Line
	9700 5600 9700 5700
Wire Wire Line
	9200 6000 9500 6000
Text Label 9200 6000 0    50   ~ 0
~FAULT
$Comp
L power:+3V3 #PWR08
U 1 1 5F1FEBF4
P 9700 5600
F 0 "#PWR08" H 9700 5450 50  0001 C CNN
F 1 "+3V3" H 9700 5750 50  0000 C CNN
F 2 "" H 9700 5600 50  0001 C CNN
F 3 "" H 9700 5600 50  0001 C CNN
	1    9700 5600
	1    0    0    -1  
$EndComp
$Comp
L power:GNDD #PWR012
U 1 1 5F1FEBEE
P 9700 6300
F 0 "#PWR012" H 9700 6050 50  0001 C CNN
F 1 "GNDD" H 9700 6150 50  0000 C CNN
F 2 "" H 9700 6300 50  0001 C CNN
F 3 "" H 9700 6300 50  0001 C CNN
	1    9700 6300
	1    0    0    -1  
$EndComp
$Comp
L Diode:BAT54S D3
U 1 1 5F1FEBE8
P 9700 6000
F 0 "D3" V 9746 6088 50  0000 L CNN
F 1 "BAT54SW" V 9655 6088 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 9775 6125 50  0001 L CNN
F 3 "https://www.onsemi.com/pub/Collateral/BAT54SWT1-D.PDF" H 9580 6000 50  0001 C CNN
F 4 "ROHM" V 9700 6000 50  0001 C CNN "Manufacturer"
F 5 "BAT54SHMFH" V 9700 6000 50  0001 C CNN "PN"
	1    9700 6000
	0    1    -1   0   
$EndComp
Text Notes 8100 4850 0    50   ~ 0
(5)
Text Notes 6300 6850 0    50   ~ 0
(5) If more space is required, use the CAT24C512 (64KiB)
$EndSCHEMATC
